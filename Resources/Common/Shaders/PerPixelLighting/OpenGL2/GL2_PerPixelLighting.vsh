//
//  Shader.vsh
//  OGLBookshelf
//
//  Created by Sascha Sanzenbacher on 25.01.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

attribute vec4 position;
attribute vec4 normal;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 normalModelViewMatrix;

varying vec4 varyingNormal;
varying vec4 varyingPosition;

void main()
{
    varyingNormal = /*normalModelViewMatrix**/normal;
    varyingPosition = /*modelViewMatrix**/position;
    gl_Position = modelViewProjectionMatrix * position;
}