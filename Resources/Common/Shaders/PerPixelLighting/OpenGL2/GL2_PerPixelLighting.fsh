//
//  Shader.fsh
//  OGLBookshelf
//
//  Created by Sascha Sanzenbacher on 25.01.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 normalModelViewMatrix;

varying vec4 varyingNormal;
varying vec4 varyingPosition;

//uniform mat4 modelViewMatrix;

void main(void)
{
    vec4 normalDirection = normalize(varyingNormal);
    vec4 eyeToLightSource = vec4(1.0, 1.0, 1.0, 0.0) - varyingPosition;
    vec4 lightDirection = normalize(vec4(1, 0, 0, 1));//vec4(normalize(eyeToLightSource.xyz), 1.0);
    
    vec4 matDiffuseColor = vec4(0.5, 0.5, 0.5, 1);
    vec4 IDiffuse = matDiffuseColor * max(0.0, dot(normalDirection.xyz, lightDirection.xyz));
    
    vec4 ISpecular;
    if (dot(normalDirection.xyz, lightDirection.xyz) <= 0.0) // light source on the wrong side?
    {
        ISpecular = vec4(0.0, 0.0, 0.0, 0.0); // no specular reflection
    }
    else // light source on the right side
    {
        vec4 matSpecularColor = vec4(1, 1, 1, 1);
        float matSpecularShininess = 1.0;
        vec4 viewDirection = normalize(vec4(modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0) - varyingPosition));
        ISpecular = matSpecularColor * pow(max(0.0, dot(reflect(-lightDirection, normalDirection).xyz, viewDirection.xyz)), matSpecularShininess);
    }
    
    gl_FragColor  = IDiffuse + ISpecular;
}