//
//  Shader.fsh
//  OGLBookshelf
//
//  Created by Sascha Sanzenbacher on 25.01.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

#version 410

#pragma optimize(off)
#pragma debug(on)

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
//uniform mat4 normalModelViewMatrix;
uniform sampler2D heightMap;

centroid in vec4 varyingNormal;
centroid in vec4 varyingPosition;
centroid in vec2 varyingTextureCoord;

out vec4 fragmentColor;

float heightAtPosition(vec2 coordinate)
{
    vec4 heightTexture = texture(heightMap, coordinate);
    return heightTexture.r;
}

vec4 normalAtPosition(vec2 coordinate)
{
    float offset = 0.08;
    vec4 normal = vec4(0.0, 0.0, 1.0, 1.0);
    
    float top = heightAtPosition(vec2(coordinate.x, clamp(coordinate.y-offset, 0.0, 1.0)));
    float bottom = heightAtPosition(vec2(coordinate.x, clamp(coordinate.y+offset, 0.0, 1.0)));
    float left = heightAtPosition(vec2(clamp(coordinate.x-offset, 0.0, 1.0), coordinate.y));
    float right = heightAtPosition(vec2(clamp(coordinate.x+offset, 0.0, 1.0), coordinate.y));
    normal = vec4((right-left)/2.0, 1.0, (bottom-top)/2.0, 1.0);
    return normal;
    return normalize(normal);
}

void main(void)
{
    vec4 normalDirection = normalAtPosition(varyingTextureCoord);
    vec4 eyeToLightSource = vec4(1.0, 1.0, 1.0, 0.0) - varyingPosition;
    vec4 lightDirection = normalize(vec4(1, 1, 0, 1));//vec4(normalize(eyeToLightSource.xyz), 1.0);
    
    vec4 matDiffuseColor = vec4(0.5, 0.5, 0.5, 1);
    vec4 IDiffuse = matDiffuseColor * max(0.0, dot(normalDirection.xyz, lightDirection.xyz));
    
    vec4 ISpecular;
    if (dot(normalDirection.xyz, lightDirection.xyz) <= 0.0) // light source on the wrong side?
    {
        ISpecular = vec4(0.0, 0.0, 0.0, 0.0); // no specular reflection
    }
    else // light source on the right side
    {
        vec4 matSpecularColor = vec4(0, 0, 0, 1);
        float matSpecularShininess = 1.0;
        vec4 viewDirection = normalize(vec4(modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0) - varyingPosition));
        ISpecular = matSpecularColor * pow(max(0.0, dot(reflect(-lightDirection, normalDirection).xyz, viewDirection.xyz)), matSpecularShininess);
    }
    
    fragmentColor  = IDiffuse + ISpecular;// + texture(heightMap, varyingTextureCoord);
//    fragmentColor = vec4(varyingTextureCoord.x,varyingTextureCoord.y, 0.0, 1.0);
//    fragmentColor = normalDirection;
}
