//
//  Shader.vsh
//  OGLBookshelf
//
//  Created by Sascha Sanzenbacher on 25.01.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

#version 410

#pragma optimize(off)
#pragma debug(on)

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec2 textureCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
//uniform mat4 normalModelViewMatrix;
uniform sampler2D heightMap;

centroid out vec4 varyingNormal;
centroid out vec4 varyingPosition;
centroid out vec2 varyingTextureCoord;

void main()
{
    vec4 heightTexture = texture(heightMap, textureCoord);
    vec4 landscapePosition = vec4(position.x, position.y + heightTexture.r*10, position.z, 1.0);
    
    varyingNormal = /*normalModelViewMatrix**/normal;
    varyingPosition = landscapePosition;
    varyingTextureCoord = textureCoord;
    
    
    gl_Position = modelViewProjectionMatrix * landscapePosition;
}
