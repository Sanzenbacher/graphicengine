/*
 *  RendererOpenGL.cpp
 *  macterrain
 *
 *  Created by Weiyun Lu on 1/8/12.
 *  Copyright 2012 Uni Stuttgart. All rights reserved.
 *
 */

#include "RendererOpenGL.h"

#include "ResourceManagment/Common/ResourceDirectories.h"

#include "RenderObject.h"
#include "CameraObject.h"
#include "RendererOpenGL_3/Common/RenderUtilityClasses/GLDebugTools.h"

#include <algorithm>
#include <OpenGL/gl3.h>

namespace RendererOpenGL_3
{
    #pragma mark Constructors and destructors
    
    RendererOpenGL::RendererOpenGL(RenderObjectFactoryBase* renderObjectFactory, SceneManager* sceneManager) :
        RendererBase::RendererBase(renderObjectFactory, sceneManager)
    {
        std::string shaderDir = ResourceDirectories::getInstance()->shaderDirectory();
        std::shared_ptr<SceneObjectProperties> properties = std::make_shared<SceneObjectProperties>();
        properties->presentation[ShaderRenderObject::PropertyPresentationFragmentShaderFilename] = shaderDir + std::string("/GL3_PerPixelLighting.fsh");
        properties->presentation[ShaderRenderObject::PropertyPresentationVertexShaderFilename] = shaderDir + std::string("/GL3_PerPixelLighting.vsh");
        m_shader = new ShaderRenderObject(properties);
        m_shader->load();
        
        const GLubyte* version = glGetString(GL_VERSION);
        GL_CHECK();
        printf("GLVersion: %s", version);
//        const GLubyte* glVersion = glGetString​(GL_SHADING_LANGUAGE_VERSION​​);
    }
    RendererOpenGL::RendererOpenGL() : RendererBase::RendererBase()
    {
    }
    RendererOpenGL::~RendererOpenGL()
    {
        m_shader->unload();
        delete m_shader;
    }
    
    void RendererOpenGL::Render(void)
    {
        glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
        GL_CHECK();
        
        glEnable(GL_DEPTH_TEST);
        GL_CHECK();
        glDepthFunc(GL_LEQUAL);
        GL_CHECK();
        glDepthMask(GL_TRUE);
        GL_CHECK();
//        glEnable(GL_TEXTURE_2D);
//        GL_CHECK();
        
        // Create light components
//        GLfloat ambientLight[] = { 0.8f, 0.2f, 0.2f, 1.0f };
//        GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
//        GLfloat specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };
//        GLfloat position[] = { 100.0f, 100.0f, 100.0f, 0.0f };
//        
//        // Assign created components to GL_LIGHT0
//        glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
//        glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
//        glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
//        glLightfv(GL_LIGHT0, GL_POSITION, position);
//        glEnable(GL_LIGHTING);
//        glEnable(GL_LIGHT0);
        
        glEnable(GL_CULL_FACE);
        GL_CHECK();
        glCullFace(GL_BACK);
        GL_CHECK();
        
        m_shader->render();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL_CHECK();
//        glMatrixMode(GL_MODELVIEW);
//        glLoadIdentity();
        
        RendererBase::Render();
        
        glFlush();
        GL_CHECK();
    }
}