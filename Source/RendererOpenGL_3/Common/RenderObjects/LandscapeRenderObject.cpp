//
//  LandscapeRenderObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 14.01.15.
//
//

#include "LandscapeRenderObject.h"

#include "Scene/Common/ScObj_SimpleTerrain.h"

#include "TextureRenderObject.h"
#include "RendererOpenGL_3/Common/RenderUtilityClasses/VertexBufferObject.h"

namespace RendererOpenGL_3
{
    const GLint cNumTrianglesRefValue = 128;
    
    LandscapeRenderObject::LandscapeRenderObject(std::shared_ptr<SceneObjectProperties> properties) :
        RenderObject(properties->identifier),
        terrainParams(new PropertyMapType(properties->presentation)),
        _heightMap(nullptr)
    {
    }
    
    LandscapeRenderObject::~LandscapeRenderObject()
    {
        if (_heightMap)
        {
            delete _heightMap;
        }
    }
    
    void LandscapeRenderObject::constructIndexBufferObject(void)
    {
        // Use detail factor to calculate number of triangles
        GLfloat detailFactor = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationDetailFactor]);
        GLfloat totalWidth = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalWidth]);
        GLfloat totalDepth = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalDepth]);
        GLint numVerticesX = cNumTrianglesRefValue * detailFactor;
        GLfloat triangleSize = ((GLfloat) totalWidth) / ((GLfloat) numVerticesX);
        GLint numVerticesZ = (GLint) (((GLfloat) totalDepth) / ((GLfloat) triangleSize));
        const GLint numOfTrianglesInQuad = 2;
        const GLint numOfVerticesInTriangle = 3;
        GLint totalNumVertices = (numVerticesX-1) * (numVerticesZ-1)*numOfTrianglesInQuad*numOfVerticesInTriangle;
        const GLint numFloatsPerVert = 8; // (x, y, z, nx, ny, nz, u, v)
        
        // Create array that holds the vertices
        GLint totalNumFloats = totalNumVertices * numFloatsPerVert;
        GLfloat* triangleVertices = new GLfloat[totalNumFloats];
        GLfloat startXCoord = 0.0f;
        GLfloat startZCoord = 0.0f;
        GLfloat currentXCoord = startXCoord;
        GLint curArrayPos = 0;
        for(GLint triangleX=0; triangleX<numVerticesX-1; triangleX++)
        {
            GLfloat currentZCoord = startZCoord;
            for(GLint triangleZ=0; triangleZ<numVerticesZ-1; triangleZ++)
            {
                GLfloat normalizedX = 0.0f;
                GLfloat normalizedZ = 0.0f;
                assert(totalNumFloats > curArrayPos);
                assert(totalNumFloats > curArrayPos+47);
                
                GLfloat currentHeight = 0.0;
                slm::vec3 vertexNormal = slm::vec3(0.0, 1.0, 0.0);
                GLfloat offset = 0.0;
                
                // First vertex coordinate
                triangleVertices[curArrayPos  ] = currentXCoord+offset;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord+offset;
                // First vertex normal
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                // First texture coordinate
                normalizedX = (currentXCoord) / totalWidth;
                normalizedZ = (currentZCoord) / totalDepth;
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
//                printf("1: %f, %f\n", currentXCoord, currentZCoord);
                
                curArrayPos += 8;
                // Second vertex coordinate
                triangleVertices[curArrayPos  ] = currentXCoord+offset;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord + triangleSize-offset;
                // Second vertex normal
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                // Second texture coordinate
                normalizedX = (currentXCoord) / totalWidth;
                normalizedZ = (currentZCoord + triangleSize) / totalDepth;
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
//                printf("1: %f, %f\n", currentXCoord, currentZCoord + triangleSize);
                
                curArrayPos += 8;
                // Third vertex coordinate
                triangleVertices[curArrayPos  ] = currentXCoord + triangleSize-offset;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord-offset;
                // Third vertex normal
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                // Third texture coordinate
                normalizedX = (currentXCoord + triangleSize) / totalWidth;
                normalizedZ = (currentZCoord) / totalDepth;
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
//                printf("1: %f, %f\n", currentXCoord + triangleSize, currentZCoord);
                
                curArrayPos += 8;
                // Fourth vertex coordinate
                triangleVertices[curArrayPos  ]  = currentXCoord+offset;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord + triangleSize-offset;
                // Fourth vertex normal
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                // Fourth texture coordinate
                normalizedX = (currentXCoord) / totalWidth;
                normalizedZ = (currentZCoord + triangleSize) / totalDepth;
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
//                printf("2: %f, %f\n", currentXCoord, currentZCoord + triangleSize);
                
                curArrayPos += 8;
                // Fifth vertex coordinate
                triangleVertices[curArrayPos  ] = currentXCoord + triangleSize-offset;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord + triangleSize-offset;
                // Fifth vertex normal
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                // Fifth texture coordinate
                normalizedX = (currentXCoord + triangleSize) / totalWidth;
                normalizedZ = (currentZCoord + triangleSize) / totalDepth;
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
//                printf("2: %f, %f\n", currentXCoord + triangleSize, currentZCoord + triangleSize);
                
                curArrayPos += 8;
                // Sixth vertex coordinate
                triangleVertices[curArrayPos  ] = currentXCoord + triangleSize-offset;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord+offset;
                // Sixth vertex normal
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                // Sixth texture coordinate
                normalizedX = (currentXCoord + triangleSize) / totalWidth;
                normalizedZ = (currentZCoord) / totalDepth;
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
//                printf("2: %f, %f\n", currentXCoord + triangleSize, currentZCoord);
                
                curArrayPos += 8;
                currentZCoord += triangleSize;
            }
            currentXCoord += triangleSize;
        }
        
        _vertexBufferObject = new VertexBufferObject();
        _vertexBufferObject->setVertexData(triangleVertices, (GLsizei)totalNumFloats*sizeof(GLfloat), 8*sizeof(GLfloat));
        _vertexBufferObject->setNormalDataOffset(3*sizeof(GLfloat));
        _vertexBufferObject->setTextureCoordsDataOffset(6*sizeof(GLfloat), 2);
    }
    
    #pragma mark Derived from RenderObject
    void LandscapeRenderObject::load(void)
    {
        // Load heightmap
        std::wstring heightmapFilename = ttl::var::get<std::wstring>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationHeightMapFilename]);
        _heightMap = new TextureRenderObject(heightmapFilename, "heightMap");
        _heightMap->load();
        
        // Setup textures
        std::vector<TextureRenderObject*> textures;
        std::vector<std::wstring> textureFilenames = ttl::var::get<std::vector<std::wstring>>((*terrainParams)[PropertyPresentationTextures]);
        for(int i=0; i<textureFilenames.size(); i++)
        {
            textures.push_back(new TextureRenderObject(textureFilenames[i]));
            addRenderObject(textures[i], RO_ORDER_BEFORE_SELF);
            textures[i]->load();
        }
        
        // Setup geometry
        constructIndexBufferObject();
    }
    void LandscapeRenderObject::render()
    {
        _heightMap->render();
        _vertexBufferObject->performRendering();
    }
    void LandscapeRenderObject::unload(void)
    {
        if (_heightMap != nullptr)
        {
            delete _heightMap;
        }
        if(_vertexBufferObject != nullptr)
        {
            delete _vertexBufferObject;
            _vertexBufferObject = nullptr;
        }
    }
}
