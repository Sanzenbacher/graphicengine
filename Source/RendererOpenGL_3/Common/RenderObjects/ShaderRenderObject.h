//
//  ShaderRenderObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 14.08.14.
//
//

#ifndef __GraphicsPlayground_Mac__ShaderRenderObject__
#define __GraphicsPlayground_Mac__ShaderRenderObject__

#include "RendererBase/Common/RenderObject.h"

namespace RendererOpenGL_3
{
    class ShaderRenderObject : public RenderObject
    {
    public:
        // Initial properties
        static const std::wstring PropertyPresentationVertexShaderFilename; // string
        static const std::wstring PropertyPresentationFragmentShaderFilename; // string
        
        ShaderRenderObject(std::shared_ptr<SceneObjectProperties> properties);
        ~ShaderRenderObject();
    
        virtual void load(void);
        virtual void render(void);
        virtual void unload(void);
        
    protected:
        bool compileShader(GLuint * shaderID, GLenum type, std::string& filename);
        bool linkShader(void);
        bool isValid(void);
        
    private:
        ShaderRenderObject();
        
        std::string m_fragmentShaderFilename;
        std::string m_vertexShaderFilename;
        
        GLuint m_program;
        GLuint m_fragmentShaderID;
        GLuint m_vertexShaderID;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__ShaderRenderObject__) */
