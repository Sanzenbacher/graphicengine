//
//  CameraObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.08.14.
//
//

#ifndef __GraphicsPlayground_Mac__MatrixTransformationObject__
#define __GraphicsPlayground_Mac__MatrixTransformationObject__

#include "RenderObject.h"

#include "Scene/Common/SceneObjectProperties.h"

namespace RendererOpenGL_3
{
    // This class defines transformations in three dimensional space
    class CameraObject : public RenderObject
    {
    public:
        // Con-/Destructor
        CameraObject(std::shared_ptr<SceneObjectProperties> sceneObjectProperties);
        virtual ~CameraObject();
        
        // Derived from RenderObject
        virtual void render(void);
        
        // Derived from ManipulatorObject
        bool manipulatePropertyWithName(std::wstring* const propertyName, void* const propertyValue);
    protected:
        void setTransformationMatrix(const slm::mat4 matrix4x4);
    private:
        slm::mat4 transformationMatrix;
        slm::mat4 inverseTransformationMatrix;
        slm::mat4 perspectiveTransformationMatrix;
        slm::mat4 combinedTransformationMatrix; // transformationMatrix * perspectiveTransformationMatrix
    };
}

#endif /* defined(__GraphicsPlayground_Mac__MatrixTransformationObject__) */
