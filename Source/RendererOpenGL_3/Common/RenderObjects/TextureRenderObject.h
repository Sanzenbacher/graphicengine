//
//  TextureRenderObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 15.01.15.
//
//

#ifndef __GraphicsPlayground_Mac__TextureRenderObject__
#define __GraphicsPlayground_Mac__TextureRenderObject__

#include "RenderObject.h"

namespace RendererOpenGL_3
{
    class TextureRenderObject : public RenderObject
    {
    public:
        TextureRenderObject(std::wstring filename);
        TextureRenderObject(std::wstring filename, std::string samplerIdentifier);
        virtual ~TextureRenderObject();
        
        // Upload data to gpu
        virtual void load(void);
        
        // Draw uploaded data with gpu
        virtual void render(void);
        
        // Unload data from gpu
        virtual void unload(void);
    protected:
        GLuint _glTextureID;
        std::wstring _filename;
        std::string _samplerIdentifier;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__TextureRenderObject__) */
