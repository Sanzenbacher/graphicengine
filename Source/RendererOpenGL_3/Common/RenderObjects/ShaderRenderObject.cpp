//
//  ShaderRenderObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 14.08.14.
//
//

#include "ShaderRenderObject.h"

#include "RendererOpenGL_3/Common/RenderUtilityClasses/GLDebugTools.h"

#include "Scene/Common/Identifier.h"

#include <OpenGL/gl3.h>

static const GLuint cGLVertexAttribPosition = 0;
static const GLuint cGLNormalAttribPosition = 1;

namespace RendererOpenGL_3
{
    const std::wstring ShaderRenderObject::PropertyPresentationVertexShaderFilename = L"PropertyPresentationVertexShaderFilename"; // wstring
    const std::wstring ShaderRenderObject::PropertyPresentationFragmentShaderFilename = L"PropertyPresentationFragmentShaderFilename"; // wstring
    
    #pragma mark - Con-/Destructors
    ShaderRenderObject::ShaderRenderObject(std::shared_ptr<SceneObjectProperties> properties) : RenderObject(Identifier(this))
    {
        m_fragmentShaderFilename = ttl::var::get<std::string>((properties->presentation)[PropertyPresentationFragmentShaderFilename]);
        m_vertexShaderFilename = ttl::var::get<std::string>((properties->presentation)[PropertyPresentationVertexShaderFilename]);
    }
    ShaderRenderObject::~ShaderRenderObject()
    {
        
    }
    #pragma mark - Derived from RenderObject
    
    void ShaderRenderObject::load(void)
    {
        RenderObject::load();
        
        m_program = glCreateProgram();
        
        bool didCompileFragmentShader = compileShader(&m_fragmentShaderID, GL_FRAGMENT_SHADER, m_fragmentShaderFilename);
        bool didCompileVertexShader = compileShader(&m_vertexShaderID, GL_VERTEX_SHADER, m_vertexShaderFilename);
        if(didCompileFragmentShader)
        {
            glAttachShader(m_program, m_fragmentShaderID);
            GL_CHECK();
        }
        if(didCompileVertexShader)
        {
            glAttachShader(m_program, m_vertexShaderID);
            GL_CHECK();
        }
        
#warning Use singleton, so the indices are gathered in one place (This shader attributePosition is used by the VBO as well) !!!
        glBindAttribLocation(m_program, cGLVertexAttribPosition, "position");
        GL_CHECK();
        glBindAttribLocation(m_program, cGLNormalAttribPosition, "normal");
        GL_CHECK();
        glBindAttribLocation(m_program, cGLNormalAttribPosition, "textureCoord");
        GL_CHECK();
        
        // Link program.
        if (!linkShader()) {
            printf("Failed to link program: %d", m_program);
            
            if (m_vertexShaderID) {
                glDeleteShader(m_vertexShaderID);
                GL_CHECK();
                m_vertexShaderID = 0;
            }
            if (m_fragmentShaderID) {
                glDeleteShader(m_fragmentShaderID);
                GL_CHECK();
                m_fragmentShaderID = 0;
            }
            if (m_program) {
                glDeleteProgram(m_program);
                GL_CHECK();
                m_program = 0;
            }
            return;
        }
        
        // Setup uniforms
        
        // Release vertex and fragment shaders.
        if (m_vertexShaderID) {
            glDetachShader(m_program, m_vertexShaderID);
            GL_CHECK();
            glDeleteShader(m_vertexShaderID);
            GL_CHECK();
            m_vertexShaderID = 0;
        }
        if (m_fragmentShaderID) {
            glDetachShader(m_program, m_fragmentShaderID);
            GL_CHECK();
            glDeleteShader(m_fragmentShaderID);
            GL_CHECK();
            m_fragmentShaderID = 0;
        }
    }
    void ShaderRenderObject::render(void)
    {
        RenderObject::render();
        
        glUseProgram(m_program);
        GL_CHECK();
    }
    void ShaderRenderObject::unload(void)
    {
        RenderObject::unload();
    }
    
    #pragma mark - Shader processing
    bool ShaderRenderObject::compileShader(GLuint * shaderID, GLenum type, std::string& filename)
    {
        // load file
        std::ifstream file (filename.c_str(), std::ios::in);
        if (file.is_open())
        {
            file.seekg (0, file.end);
            size_t size = file.tellg();
            size++;
            char* memblock = new char [size];
            file.seekg (0, std::ios::beg);
            file.read (memblock, size);
            file.close();
            memblock[size-1] = '\0';
            
            *shaderID = glCreateShader(type);
            glShaderSource(*shaderID, 1, &memblock, NULL);
            glCompileShader(*shaderID);
            
#if defined(DEBUG)
            GLint logLength;
            glGetShaderiv(*shaderID, GL_INFO_LOG_LENGTH, &logLength);
            GL_CHECK();
            if (logLength > 0) {
                GLchar *log = (GLchar *)malloc(logLength);
                glGetShaderInfoLog(*shaderID, logLength, &logLength, log);
                GL_CHECK();
                printf("Shader (%s) compile log:\n%s", filename.c_str(), log);
                free(log);
            }
#endif
            GLint status;
            glGetShaderiv(*shaderID, GL_COMPILE_STATUS, &status);
            GL_CHECK();
            if (status == 0)
            {
                glDeleteShader(*shaderID);
                GL_CHECK();
                delete[] memblock;
                return false;
            }

            delete[] memblock;
            
            return true;
        }
        else
        {
            printf("Failed to load shader: File doesn't exist");
            return false;
        }
    }
    bool ShaderRenderObject::linkShader(void)
    {
        GLint status;
        glLinkProgram(m_program);
        GL_CHECK();
        
#if defined(DEBUG)
        GLint logLength;
        glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logLength);
        GL_CHECK();
        if (logLength > 0) {
            GLchar *log = (GLchar *)malloc(logLength);
            glGetProgramInfoLog(m_program, logLength, &logLength, log);
            GL_CHECK();
            printf("Program link log:\n%s", log);
            free(log);
        }
#endif
        
        glGetProgramiv(m_program, GL_LINK_STATUS, &status);
        GL_CHECK();
        if (status == 0)
        {
            return false;
        }
        
        return true;
    }
    bool ShaderRenderObject::isValid(void)
    {
        GLint logLength, status;
        
        glValidateProgram(m_program);
        GL_CHECK();
        glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logLength);
        GL_CHECK();
        if (logLength > 0) {
            GLchar *log = (GLchar *)malloc(logLength);
            glGetProgramInfoLog(m_program, logLength, &logLength, log);
            GL_CHECK();
            printf("Program validate log:\n%s", log);
            free(log);
        }
        
        glGetProgramiv(m_program, GL_VALIDATE_STATUS, &status);
        GL_CHECK();
        if (status == 0)
        {
            return false;
        }
        
        return true;
    }
}
