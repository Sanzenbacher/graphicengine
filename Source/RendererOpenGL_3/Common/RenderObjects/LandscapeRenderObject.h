//
//  LandscapeRenderObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 14.01.15.
//
//

#ifndef __GraphicsPlayground_Mac__LandscapeRenderObject__
#define __GraphicsPlayground_Mac__LandscapeRenderObject__

#include "RenderObject.h"
#include "SceneObjectProperties.h"
#include "TextureRenderObject.h"

namespace RendererOpenGL_3
{
    class VertexBufferObject;
    
    class LandscapeRenderObject : public RenderObject
    {
    public:
        // Con-/Destructors
        LandscapeRenderObject(std::shared_ptr<SceneObjectProperties> description);
        ~LandscapeRenderObject();
        
        virtual void load(void);
        virtual void render(void);
        virtual void unload(void);
    protected:
        LandscapeRenderObject();
        void constructIndexBufferObject(void);
        
        std::shared_ptr<PropertyMapType> terrainParams;
        TextureRenderObject* _heightMap;
        VertexBufferObject* _vertexBufferObject;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__LandscapeRenderObject__) */
