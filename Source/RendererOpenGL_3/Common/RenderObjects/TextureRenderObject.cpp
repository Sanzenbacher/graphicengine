//
//  TextureRenderObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 15.01.15.
//
//

#include "TextureRenderObject.h"

#include "ResourceManagment/Common/TextureManager.h"
#include "ResourceManagment/Common/TextureDataBase.h"

#include "GLDebugTools.h"
#include <OpenGL/gl3.h>

namespace RendererOpenGL_3
{
    TextureRenderObject::TextureRenderObject(std::wstring filename) :
       RenderObject(Identifier(this)),
        _glTextureID(666),
        _filename(filename),
        _samplerIdentifier("")
   {
       
   }

    TextureRenderObject::TextureRenderObject(std::wstring filename, std::string samplerIdentifier) :
        RenderObject(Identifier(this)),
        _glTextureID(666),
        _filename(filename),
        _samplerIdentifier(samplerIdentifier)
    {
        
    }

    TextureRenderObject::~TextureRenderObject()
    {
        
    }
    
    // Upload data to gpu
    void TextureRenderObject::load(void)
    {
        TextureDataBase* textureData = TextureManager::getInstance()->loadTexture(_filename);
        
        glGenTextures( 1, &_glTextureID);
        GL_CHECK();
        glBindTexture( GL_TEXTURE_2D, _glTextureID);
        GL_CHECK();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        GL_CHECK();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
        GL_CHECK();
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        GL_CHECK();
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        GL_CHECK();
        glTexImage2D( GL_TEXTURE_2D, 0, textureData->format, textureData->width,
                     textureData->height, 0, textureData->format,
                     GL_UNSIGNED_BYTE, textureData->bitmapData);
        GL_CHECK();
        
        GLint programID;
        glGetIntegerv(GL_CURRENT_PROGRAM, &programID);
        GL_CHECK();
        GLint heightmapID = glGetUniformLocation(programID, _samplerIdentifier.c_str());
        GL_CHECK();
        const int TEXTURE_UNIT_ZERO = 0;
        glActiveTexture( GL_TEXTURE0 + TEXTURE_UNIT_ZERO );
        GL_CHECK();
        glBindTexture( GL_TEXTURE_2D, _glTextureID);
        GL_CHECK();
        glUniform1i( heightmapID, TEXTURE_UNIT_ZERO );
        GL_CHECK();
        
        delete textureData;
    }
    
    // Draw uploaded data with gpu
    void TextureRenderObject::render(void)
    {
        if (_samplerIdentifier != "")
        {
            const int TEXTURE_UNIT_ZERO = 0;
            glActiveTexture( GL_TEXTURE0 + TEXTURE_UNIT_ZERO );
            GL_CHECK();
            glBindTexture( GL_TEXTURE_2D, _glTextureID);
            GL_CHECK();
        }
    }
    
    // Unload data from gpu
    void TextureRenderObject::unload(void)
    {
        glDeleteTextures(1, &_glTextureID);
        GL_CHECK();
    }
}
