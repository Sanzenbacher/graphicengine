//
//  CameraObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.08.14.
//
//

#include "CameraObject.h"

#include "Scene/Common/ScObj_SimpleCamera.h"

#include <OpenGL/gl3.h>

namespace RendererOpenGL_3
{
#pragma mark Con-/Destructor
    CameraObject::CameraObject(std::shared_ptr<SceneObjectProperties> sceneObjectProperties) :
    RenderObject(sceneObjectProperties->identifier)
    {
        transformationMatrix = ttl::var::get<slm::mat4>(sceneObjectProperties->presentation[PropertyPresentationTransformation]);
        inverseTransformationMatrix = slm::inverse(transformationMatrix);
        try
        {
            perspectiveTransformationMatrix = ttl::var::get<slm::mat4>(sceneObjectProperties->presentation[ScObj_SimpleCamera::PropertyPresentationPerspectiveTransformation]);
        }
        catch(ttl::var::exception notFoundException)
        {
            perspectiveTransformationMatrix = slm::mat4(1);
        }
        combinedTransformationMatrix = perspectiveTransformationMatrix*transformationMatrix;
    }
    CameraObject::~CameraObject()
    {
    }
    
#pragma mark Setters
    void CameraObject::setTransformationMatrix(const slm::mat4 matrix4x4)
    {
#warning Need model, view and perspective matrix !!! model matrix has to be set by StaticMeshRenderObject !!!
        transformationMatrix = matrix4x4;
        inverseTransformationMatrix = slm::inverse(matrix4x4);
        combinedTransformationMatrix = perspectiveTransformationMatrix*transformationMatrix;
    }
    
#pragma mark Derived from RenderObject
    void CameraObject::render(void)
    {
        GLint programID;
        glGetIntegerv(GL_CURRENT_PROGRAM, &programID);
        
        if(programID != 0)
        {
            GLint modelViewProjectionUniformLocation = glGetUniformLocation(programID, "modelViewProjectionMatrix");
            if(modelViewProjectionUniformLocation != -1)
            {
                glUniformMatrix4fv(modelViewProjectionUniformLocation, 1, false, combinedTransformationMatrix.begin());
            }
            else
            {
                printf("CameraObject: Didn't find uniform with name \"modelViewProjectionMatrix\"\n");
            }
            
//            GLint normalUniformLocation = glGetUniformLocation(programID, "normalModelViewMatrix");
//            if(normalUniformLocation != -1)
//            {
//                glUniformMatrix4fv(normalUniformLocation, 1, false, inverseTransformationMatrix.begin());
//            }
//            else
//            {
//                printf("CameraObject: Didn't find uniform with name \"normalModelViewMatrix\"\n");
//            }
            
            GLint modelViewUniformLocation = glGetUniformLocation(programID, "modelViewMatrix");
            if(modelViewUniformLocation != -1)
            {
                glUniformMatrix4fv(modelViewUniformLocation, 1, false, transformationMatrix.begin());
            }
            else
            {
                printf("CameraObject: Didn't find uniform with name \"modelViewMatrix\"\n");
            }
            
            // Check matrix value
//            slm::mat4 matTest = slm::mat4(1);
//            glGetUniformfv(programID, modelViewUniformLocation, matTest.begin());
        }
    }
    
#pragma mark Derived from ManipulatorObject
    bool CameraObject::manipulatePropertyWithName(std::wstring* const propertyName, void* const propertyValue)
    {
        bool isHandled = false;
        if(propertyName == cMPTransformationMatrix)
        {
            setTransformationMatrix(*(slm::mat4*)propertyValue);
            isHandled = true;
        }
        return isHandled;
    }
}
