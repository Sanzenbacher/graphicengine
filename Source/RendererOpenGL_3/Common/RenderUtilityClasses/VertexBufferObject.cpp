//
//  VertexBufferObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 21.07.14.
//
//

#include "VertexBufferObject.h"
#include "GLDebugTools.h"

#include <OpenGL/gl3.h>

namespace RendererOpenGL_3
{
    VertexBufferObject::VertexBufferObject(void) :
    _VBOID(0),
    _InterleavedData(nullptr),
    _InterleavedNumBytes(0),
    _VertexStride(0),
    _NormalDataOffset(-1),
    _TextureCoordOffset(-1),
    _VertexArrayID(0)
    {
        
    }
    
    VertexBufferObject::~VertexBufferObject(void)
    {
        glDeleteBuffers(1, &_VBOID);
        
        if(_InterleavedData != nullptr)
        {
            delete [] (GLfloat*)_InterleavedData;
        }
    }
    
    void VertexBufferObject::setVertexData(GLvoid* vertexData, GLsizei numBytes, GLuint vertexStride)
    {
        _InterleavedData = vertexData;
        _InterleavedNumBytes = numBytes;
        _VertexStride = vertexStride;
        
        glGenVertexArrays(1, &_VertexArrayID);
        GL_CHECK();
        glBindVertexArray(_VertexArrayID);
        GL_CHECK();
        
        glGenBuffers(1, &_VBOID);
        GL_CHECK();
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        GL_CHECK();
        glBufferData(GL_ARRAY_BUFFER, _InterleavedNumBytes, _InterleavedData, GL_STATIC_DRAW);
        GL_CHECK();
        
#warning Use singleton, so the indices are gathered in one place (This shader attributePosition is used by the ShaderRenderObject as well) !!!
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        static const int vertexAttributePosition = 0;

        glEnableVertexAttribArray(vertexAttributePosition);
        GL_CHECK();
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        GL_CHECK();
        glVertexAttribPointer(vertexAttributePosition, 3, GL_FLOAT, GL_FALSE, _VertexStride, 0);
        GL_CHECK();
        glBindVertexArray(0);
        GL_CHECK();
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    void VertexBufferObject::setNormalDataOffset(GLint offset)
    {
        _NormalDataOffset = offset;
        
        glBindVertexArray(_VertexArrayID);
        
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        GL_CHECK();
        
        static const int vertexAttributePosition = 1;

        glEnableVertexAttribArray(vertexAttributePosition);
        GL_CHECK();
        glVertexAttribPointer(vertexAttributePosition, 3, GL_FLOAT, GL_FALSE, _VertexStride, ((char*)nullptr)+_NormalDataOffset);
        GL_CHECK();
        
        glBindVertexArray(0);
    }
    
    void VertexBufferObject::setTextureCoordsDataOffset(GLint offset, GLint coordDim)
    {
        _TextureCoordOffset = offset;
        
        glBindVertexArray(_VertexArrayID);
        
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        GL_CHECK();
        
        static const int vertexAttributePosition = 2;
        glEnableVertexAttribArray(vertexAttributePosition);
        GL_CHECK();
        glVertexAttribPointer(vertexAttributePosition, 2, GL_FLOAT, GL_FALSE, _VertexStride, ((char*)nullptr)+_TextureCoordOffset);
        GL_CHECK();
    }
    
    void VertexBufferObject::performRendering(void)
    {
//        glEnableVertexAttribArray(_VertexArrayID);
//        GL_CHECK();
//
        glBindVertexArray(_VertexArrayID);
        GL_CHECK();
        
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        GL_CHECK();
        
        glDrawArrays(GL_TRIANGLES, 0, _InterleavedNumBytes / (_VertexStride));
        GL_CHECK();
        
        glBindVertexArray(0);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
//        glDisableVertexAttribArray(_VertexArrayID);
    }
}
