//
//  GLDebugTools.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 08.01.15.
//
//

#ifndef __GraphicsPlayground_Mac__GLDebugTools__
#define __GraphicsPlayground_Mac__GLDebugTools__

#import <stdio.h>
#import <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif
    void CheckOpenGLError(const char* fname, int line);
#ifdef __cplusplus
}
#endif

#ifdef DEBUG
#define GL_CHECK() do { \
CheckOpenGLError(__FILE__, __LINE__); \
} while (0)
#else
#define GL_CHECK()
#endif

#endif /* defined(__GraphicsPlayground_Mac__GLDebugTools__) */
