//
//  GLDebugTools.c
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 08.01.15.
//
//

#include "GLDebugTools.h"

#include <OpenGL/gl3.h>

void CheckOpenGLError(const char* fname, int line)
{
    GLenum err = glGetError();
    if (err != GL_NO_ERROR)
    {
        printf("OpenGL error %08x, at %s:%i\n", err, fname, line);
//        abort();
    }
}