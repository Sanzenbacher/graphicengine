//
//  RenderObjectFactoryOpenGL.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 22.06.14.
//
//

#include "RenderObjectFactoryOpenGL.h"

#include "LandscapeRenderObject.h"
#include "TriangleObject.h"
#include "RendererOpenGL_3/Common/RenderObjects/CameraObject.h"
#include "RendererOpenGL_3/Common/RenderObjects/StaticMeshRenderObject.h"
#include "RendererOpenGL_3/Common/RenderObjects/LandscapeRenderObject.h"

class SceneObjectProperties;

namespace RendererOpenGL_3
{
    RenderObject* RenderObjectFactoryOpenGL::CreateRenderObject(std::shared_ptr<SceneObjectProperties> properties)
    {
        RenderObject* renderObject = NULL;
        PresentationType presentationType = ttl::var::get<PresentationType>(properties->presentation[PropertyPresentationType]);
        switch (presentationType) {
//            case PresentationType::PT_TYPE_TRIANGLE:
//                renderObject = new RendererOpenGL_2::TriangleObj(properties);
//                break;
            case PresentationType::PT_TYPE_STATIC_HIGHTMAP:
                renderObject = new RendererOpenGL_3::LandscapeRenderObject(properties);
                break;
            case PresentationType::PT_TYPE_CAMERA:
                renderObject = new RendererOpenGL_3::CameraObject(properties);
                break;
            case PresentationType::PT_TYPE_STATIC_MESH:
                renderObject = new RendererOpenGL_3::StaticMeshRenderObject(properties);
                break;
            default:
                break;
//                assert(false); // RenderObject type not defined
        }
//        assert(renderObject != NULL);
        InitializeRenderObject(renderObject, properties);
        return renderObject;
    }
}