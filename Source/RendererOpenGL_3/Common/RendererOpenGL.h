/*
 *  RendererOpenGL.h
 *  macterrain
 *
 *  This class holds properties that describes proberties which are used to
 *  generate an image by the Renderer-Object.
 *
 *  Created by Weiyun Lu on 1/8/12.
 *  Copyright 2012 Uni Stuttgart. All rights reserved.
 *
 */

#pragma once

#include <vector>

#include "RendererBase.h"

#include "Scene/Common/SceneObjBase.h"

#include "RenderObjects/ShaderRenderObject.h"

// Forward declarations
class RenderObject;
class CameraObject;

namespace RendererOpenGL_3
{
    // Type definitions
    typedef std::vector<RenderObject*> RenderObjectVector;
    
    // This class is responsible for rendering objects.
    // It offers functionality to register buffer objects that will be rendered.
    class RendererOpenGL : public RendererBase
    {
    public:
        // Constructor and Destructor
        RendererOpenGL(RenderObjectFactoryBase* renderObjectFactory, SceneManager* sceneManager);
        virtual ~RendererOpenGL();
        
        virtual void Render(void);
        
    protected:
        ShaderRenderObject* m_shader;

    private:
        // Private constructor
        RendererOpenGL();
    };
}