//
//  RenderObjectFactoryOpenGL.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 22.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__RenderObjectFactoryOpenGL__
#define __GraphicsPlayground_Mac__RenderObjectFactoryOpenGL__

#include <iostream>

#include "RendererBase/Common/RenderObjectFactoryBase.h"

#include "Scene/Common/SceneObjectProperties.h"

namespace RendererOpenGL_2
{
    class RenderObjectFactoryOpenGL : public RenderObjectFactoryBase
    {
    public:
        virtual RenderObject* CreateRenderObject(std::shared_ptr<SceneObjectProperties> description);
    };
}

#endif /* defined(__GraphicsPlayground_Mac__RenderObjectFactoryOpenGL__) */
