//
//  StaticMeshRenderObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.07.14.
//
//

#include "StaticMeshRenderObject.h"

#include "Scene/Common/SceneObjectProperties.h"
#include "Scene/Common/ScObj_StaticMesh.h"

#include "ResourceManagment/Common/ModelManager.h"

#include "RendererOpenGL_2/Common/RenderUtilityClasses/VertexBufferObject.h"

namespace RendererOpenGL_2
{
    StaticMeshRenderObject::StaticMeshRenderObject(std::shared_ptr<SceneObjectProperties> properties) :
    RenderObject(properties->identifier),
    _meshFilename(ttl::var::get<std::string>((properties->presentation)[ScObj_StaticMesh::PropertyPresentationStaticMeshFilename])),
    _meshData(nullptr),
    _vertexBufferObject(nullptr)
    {
    }
    
    StaticMeshRenderObject::~StaticMeshRenderObject()
    {
        
    }
    
    void StaticMeshRenderObject::load(void)
    {
        _meshData = ModelManager::getInstance()->loadModel(_meshFilename);
        _vertexBufferObject = new VertexBufferObject();
        float* interleavedMeshData = _meshData->interleavedArray();
        GLsizei vertexSize = (3+3)*sizeof(GLfloat);
        _vertexBufferObject->setVertexData(interleavedMeshData, (GLsizei)(_meshData->Triangles.size())*3*vertexSize, vertexSize);
        _vertexBufferObject->setNormalDataOffset(3*sizeof(GLfloat));
        //    _vertexBufferObject->setTextureCoordsDataOffset(GLint offset, <#GLint coordDim#>)
    }
    void StaticMeshRenderObject::render(void)
    {
        _vertexBufferObject->performRendering();
    }
    void StaticMeshRenderObject::unload(void)
    {
        if(_vertexBufferObject != nullptr)
        {
            delete _vertexBufferObject;
            _vertexBufferObject = nullptr;
        } 
    }
}