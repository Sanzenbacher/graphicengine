//
//  CameraObject.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 18.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_MatrixTransformationObject_h
#define macterrain_MatrixTransformationObject_h

#include "RenderObject.h"

#include "Scene/Common/SceneObjectProperties.h"

namespace RendererOpenGL_2
{
    // This class defines transformations in three dimensional space
    class CameraObject : public RenderObject
    {
    public:
        // Con-/Destructor
        CameraObject(std::shared_ptr<SceneObjectProperties> sceneObjectProperties);
        virtual ~CameraObject();
        
        // Derived from RenderObject
        virtual void render(void);
        
        // Derived from ManipulatorObject
        bool manipulatePropertyWithName(std::wstring* const propertyName, void* const propertyValue);
    protected:
        void setTransformationMatrix(const slm::mat4 matrix4x4);
    private:
        //GLfloat* transformationMatrix;
        slm::mat4   transformationMatrix;
    };
}
#endif
