//
//  StaticMeshRenderObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__StaticMeshRenderObject__
#define __GraphicsPlayground_Mac__StaticMeshRenderObject__

#include "ResourceManagment/Common/ModelData.h"

#include "RenderObject.h"

class SceneObjectProperties;

namespace RendererOpenGL_2
{
    class VertexBufferObject;
    
    class StaticMeshRenderObject : public RenderObject
    {
    public:
        StaticMeshRenderObject(std::shared_ptr<SceneObjectProperties> properties);
        ~StaticMeshRenderObject();
        
        virtual void load(void);
        virtual void render(void);
        virtual void unload(void);
        
    protected:
        std::string _meshFilename;
        ModelLoader::ModelData*  _meshData;
        VertexBufferObject* _vertexBufferObject;
        
    private:
        StaticMeshRenderObject();
    };
}
#endif /* defined(__GraphicsPlayground_Mac__StaticMeshRenderObject__) */
