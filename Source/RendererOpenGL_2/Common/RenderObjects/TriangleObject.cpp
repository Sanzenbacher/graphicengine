//
//  TriangleObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.06.14.
//
//

#include "TriangleObject.h"

#include <OpenGL/gl.h>

#include "ResourceManagment/Common/TextureManager.h"

#include "TextureRenderObject.h"

namespace RendererOpenGL_2
{
    TriangleObj::TriangleObj(std::shared_ptr<SceneObjectProperties> properties) :
    RenderObject(properties->identifier)
    {
        _properties = properties;
    }
    
    TriangleObj::~TriangleObj()
    {
    }
    
    void TriangleObj::load(void)
    {
        RenderObject::load();
        
        std::vector<std::wstring> textureFilenames = ttl::var::get<std::vector<std::wstring>>(_properties->presentation[PropertyPresentationTextures]);
        for(int i=0; i<textureFilenames.size(); i++)
        {
            TextureRenderObject* textureRO = new TextureRenderObject(textureFilenames[i]);
            textures.push_back(textureRO);
            textureRO->load();
            addRenderObject(textureRO, RO_ORDER_BEFORE_SELF);
        }
    }
    void TriangleObj::render(void)
    {
        RenderObject::render();
        
        glBegin(GL_TRIANGLES);
        
        glTexCoord2f(0.0, 0.0);
        glVertex3f(0, 0, 0);
        
        glTexCoord2f(1.0, 0.0);
        glVertex3f(0, 1, 0);
        
        glTexCoord2f(0.0, 1.0);
        glVertex3f(1, 0, 0);
        glEnd();
    }
    void TriangleObj::unload(void)
    {
        RenderObject::unload();
    }
}