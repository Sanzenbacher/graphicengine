//
//  TextureRenderObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 07.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__TextureRenderObject__
#define __GraphicsPlayground_Mac__TextureRenderObject__

#include <iostream>

#include "RenderObject.h"

namespace RendererOpenGL_2
{
    class TextureRenderObject : public RenderObject
    {
    public:
        TextureRenderObject(std::wstring filename);
        virtual ~TextureRenderObject();
        
        // Upload data to gpu
        virtual void load(void);
        
        // Draw uploaded data with gpu
        virtual void render(void);
        
        // Unload data from gpu
        virtual void unload(void);
    protected:
        GLuint _glTextureID;
        std::wstring _filename;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__TextureRenderObject__) */
