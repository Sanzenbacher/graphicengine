//
//  IndexedBufferObject.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 15.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "LandscapeRenderObject.h"

#include <iostream>
#include <OpenGL/gl.h>

#include "Scene/Common/ScObj_SimpleTerrain.h"

#include "ResourceManagment/Common/TextureManager.h"
#include "ResourceManagment/Common/TextureDataBase.h"

#include "TextureRenderObject.h"
#include "RendererOpenGL_2/Common/RenderUtilityClasses/InterleavedIndexBuffer.h"

class SceneObjectProperties;

namespace RendererOpenGL_2
{
    const GLint cNumTrianglesRefValue = 64;
    
#pragma mark Con-/Destructors
    
    LandscapeRenderObject::LandscapeRenderObject(std::shared_ptr<SceneObjectProperties> properties) :
    RenderObject(properties->identifier),
    terrainParams(new PropertyMapType(properties->presentation)),
    heightMap(nullptr),
    _terrainIndexBuffer(nullptr)
    {
    }
    LandscapeRenderObject::~LandscapeRenderObject()
    {
        if(heightMap != nullptr)
        {
            delete heightMap;
        }
        
        if(_terrainIndexBuffer != nullptr)
        {
            delete _terrainIndexBuffer;
        }
    }
    
#pragma mark Derived from RenderObject
    // Upload data to gpu
    void LandscapeRenderObject::load(void)
    {
        // Setup textures
        std::wstring heightmapFilename = ttl::var::get<std::wstring>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationHeightMapFilename]);
        heightMap = TextureManager::getInstance()->loadTexture(heightmapFilename);
        
        std::vector<TextureRenderObject*> textures;
        std::vector<std::wstring> textureFilenames = ttl::var::get<std::vector<std::wstring>>((*terrainParams)[PropertyPresentationTextures]);
        for(int i=0; i<textureFilenames.size(); i++)
        {
            textures.push_back(new TextureRenderObject(textureFilenames[i]));
            addRenderObject(textures[i], RO_ORDER_BEFORE_SELF);
            textures[i]->load();
        }
        
        // Setup geometry
        constructIndexBufferObject();
        
        RenderObject::load();
    }
    
    void LandscapeRenderObject::render()
    {
        RenderObject::render();
        
        if(_terrainIndexBuffer != nullptr)
        {
            _terrainIndexBuffer->performRendering();
        }
    }
    
    // Unload data from gpu
    void LandscapeRenderObject::unload(void)
    {
        RenderObject::unload();
        
        if(heightMap != nullptr)
        {
            delete heightMap;
            heightMap = nullptr;
        }
        
        if(_terrainIndexBuffer != nullptr)
        {
            delete _terrainIndexBuffer;
            _terrainIndexBuffer = nullptr;
        }
    }
    
#pragma mark Methods for the terrain
    GLfloat LandscapeRenderObject::getHeight(GLfloat normalizedX, GLfloat normalizedY)
    {
        // Calculate height from the heightmap
        //GLfloat normalizedX = (currentXCoord) / params.totalWidth;
        //GLfloat normalizedY = (currentZCoord) / params.totalDepth;
        normalizedX = slm::clamp(normalizedX, 0.0f, 1.0f);
        normalizedY = slm::clamp(normalizedY, 0.0f, 1.0f);
        RgbaColor currentColor = heightMap->getPixelInterpolatedColor(normalizedX, normalizedY);
        GLfloat currentHeight = (currentColor.red);
        GLfloat totalHeight = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalHeight]);
        return currentHeight*totalHeight;
    }
    
    slm::vec3 LandscapeRenderObject::getNormal(GLfloat currentX, GLfloat currentZ, GLfloat normalizedHeight,
                                               GLfloat triangleSize)
    {
        // this algorithm is inspired by http://www.flipcode.com/archives/Calculating_Vertex_Normals_for_Height_Maps.shtml
        
        GLfloat totalWidth = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalWidth]);
        GLfloat totalDepth = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalDepth]);
        
        GLfloat sx = getHeight((currentX + triangleSize)/totalWidth, currentZ/totalDepth) -
        getHeight((currentX - triangleSize)/totalWidth, currentZ/totalDepth);
        GLfloat sy = getHeight(currentX/totalWidth, (currentZ + triangleSize)/totalDepth) -
        getHeight(currentX/totalWidth, (currentZ - triangleSize)/totalDepth);
        
        slm::vec3 normal(-sx, 1.0, -sy);
        normal.normalize();
        return normal;
    }
    
    void LandscapeRenderObject::constructIndexBufferObject(void)
    {
        // Use detail factor to calculate number of triangles
        GLfloat detailFactor = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationDetailFactor]);
        GLfloat totalWidth = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalWidth]);
        GLfloat totalDepth = ttl::var::get<float>((*terrainParams)[ScObj_SimpleTerrain::PropertyPresentationTotalDepth]);
        GLint numVerticesX = cNumTrianglesRefValue * detailFactor;
        GLfloat triangleSize = ((GLfloat) totalWidth) / ((GLfloat) numVerticesX);
        GLint numVerticesZ = (GLint) (((GLfloat) totalDepth) / ((GLfloat) triangleSize));
        GLint totalNumVertices = numVerticesX * numVerticesZ;
        const GLint numFloatsPerVert = 8; // (x, y, z, nx, ny, nz, u, v)
        
        // Create array that holds the vertices
        GLint totalNumFloats = totalNumVertices * numFloatsPerVert;
        GLfloat* triangleVertices = new GLfloat[totalNumFloats];
        GLfloat startXCoord = 0.0f;
        GLfloat startZCoord = 0.0f;
        GLfloat currentXCoord = startXCoord;
        for(GLint triangleX=0; triangleX<numVerticesX; triangleX++)
        {
            GLfloat currentZCoord = startZCoord;
            for(GLint triangleZ=0; triangleZ<numVerticesZ; triangleZ++)
            {
                GLint curArrayPos = (triangleX*numVerticesZ + triangleZ) * numFloatsPerVert;
                GLfloat normalizedX = (currentXCoord) / totalWidth;
                GLfloat normalizedZ = (currentZCoord) / totalDepth;
                assert(totalNumFloats > curArrayPos);
                assert(totalNumFloats > curArrayPos+5);
                
                // Setup the vertex coords
                GLfloat currentHeight = getHeight(normalizedX, normalizedZ);
                triangleVertices[curArrayPos]   = currentXCoord;
                triangleVertices[curArrayPos+1] = currentHeight;
                triangleVertices[curArrayPos+2] = currentZCoord;
                
                // Setup the vertex normal
                slm::vec3 vertexNormal = getNormal(currentXCoord, currentZCoord,
                                                   currentHeight, triangleSize);
                triangleVertices[curArrayPos+3] = vertexNormal.x;
                triangleVertices[curArrayPos+4] = vertexNormal.y;
                triangleVertices[curArrayPos+5] = vertexNormal.z;
                
                // Setup texture coordinates
                triangleVertices[curArrayPos+6] = normalizedX;
                triangleVertices[curArrayPos+7] = normalizedZ;
                
                currentZCoord += triangleSize;
            }
            currentXCoord += triangleSize;
        }
        
        // Initialize an IndexBufferedObject
        _terrainIndexBuffer = new InterleavedIndexBuffer();
        _terrainIndexBuffer->setVertexData(triangleVertices, (GLsizei)totalNumFloats*(GLsizei)sizeof(GLfloat), numFloatsPerVert*sizeof(GLfloat));
        _terrainIndexBuffer->setNormalDataOffset(3*sizeof(GLfloat));
        _terrainIndexBuffer->setTextureCoordsDataOffset(6*sizeof(GLfloat));
        
        // Create array that holds the indices
        GLint numQuadsX = (numVerticesX - 1);
        GLint numQuadsZ = (numVerticesZ - 1);
        GLsizei totalNumIndices = numQuadsX *numQuadsZ * 4;
        GLint* quadIndices = new GLint[totalNumIndices];
        for(GLint quadX=0; quadX<numQuadsX; quadX++)
        {
            for(GLint quadZ=0; quadZ<numQuadsZ; quadZ++)
            {
                GLint currentQuadNum = quadX*numQuadsZ + quadZ;
                quadIndices[currentQuadNum*4] = quadX*numVerticesZ+quadZ;
                quadIndices[currentQuadNum*4+1] = quadX*numVerticesZ+quadZ+1;
                quadIndices[currentQuadNum*4+2] = (quadX+1)*numVerticesZ+quadZ+1;
                quadIndices[currentQuadNum*4+3] = (quadX+1)*numVerticesZ+quadZ;
                
            }
        }
        _terrainIndexBuffer->setIndexData(quadIndices, totalNumIndices*(GLsizei)sizeof(GLint));
    }
}
