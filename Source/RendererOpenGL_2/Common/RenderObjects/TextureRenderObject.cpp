//
//  TextureRenderObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 07.07.14.
//
//

#include "TextureRenderObject.h"

#include "ResourceManagment/Common/TextureManager.h"
#include "ResourceManagment/Common/TextureDataBase.h"

#include <OpenGL/gl.h>

namespace RendererOpenGL_2
{
    TextureRenderObject::TextureRenderObject(std::wstring filename) : RenderObject(Identifier(this)),
    _filename(filename)
    {
        
    }
    TextureRenderObject::~TextureRenderObject()
    {
        
    }
    
    // Upload data to gpu
    void TextureRenderObject::load(void)
    {
        TextureDataBase* textureData = TextureManager::getInstance()->loadTexture(_filename);
        
        glGenTextures( 1, &_glTextureID);
        glBindTexture( GL_TEXTURE_2D, _glTextureID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexImage2D( GL_TEXTURE_2D, 0, textureData->format, textureData->width,
                     textureData->height, 0, textureData->format,
                     GL_UNSIGNED_BYTE, textureData->bitmapData);
        
        delete textureData;
    }
    
    // Draw uploaded data with gpu
    void TextureRenderObject::render(void)
    {
        glBindTexture( GL_TEXTURE_2D, _glTextureID );
    }
    
    // Unload data from gpu
    void TextureRenderObject::unload(void)
    {
        glDeleteTextures(1, &_glTextureID);
    }
}