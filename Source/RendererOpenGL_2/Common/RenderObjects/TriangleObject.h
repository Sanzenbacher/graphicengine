//
//  TriangleObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__TriangleObject__
#define __GraphicsPlayground_Mac__TriangleObject__

#include <iostream>

#include "Scene/Common/ScObj_Triangle.h"

#include "RendererBase/Common/RenderObject.h"

class SceneObjectProperties;

namespace RendererOpenGL_2
{
    class TextureRenderObject;
    
    class TriangleObj : public RenderObject
    {
    public:
        TriangleObj(std::shared_ptr<SceneObjectProperties> properties);
        virtual ~TriangleObj();
        
        // Upload data to gpu
        virtual void load(void);
        
        // Draw uploaded data with gpu
        virtual void render(void);
        
        // Unload data from gpu
        virtual void unload(void);
    protected:
        std::shared_ptr<SceneObjectProperties> _properties;
        std::vector<TextureRenderObject*> textures;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__TriangleObject__) */
