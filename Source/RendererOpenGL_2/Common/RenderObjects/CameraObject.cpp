//
//  CameraObject.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 18.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "CameraObject.h"

#include <OpenGL/gl.h>

namespace RendererOpenGL_2
{
#pragma mark Con-/Destructor
    CameraObject::CameraObject(std::shared_ptr<SceneObjectProperties> sceneObjectProperties) :
    RenderObject(sceneObjectProperties->identifier)
    {
        transformationMatrix = ttl::var::get<slm::mat4>(sceneObjectProperties->presentation[PropertyPresentationTransformation]);
    }
    CameraObject::~CameraObject()
    {
    }
    
#pragma mark Setters
    void CameraObject::setTransformationMatrix(const slm::mat4 matrix4x4)
    {
        transformationMatrix = matrix4x4;
    }
    
#pragma mark Derived from RenderObject
    void CameraObject::render(void)
    {
        glMultMatrixf(transformationMatrix.begin());
    }
    
#pragma mark Derived from ManipulatorObject
    bool CameraObject::manipulatePropertyWithName(std::wstring* const propertyName, void* const propertyValue)
    {
        bool isHandled = false;
        if(propertyName == cMPTransformationMatrix)
        {
            setTransformationMatrix(*(slm::mat4*)propertyValue);
            isHandled = true;
        }
        return isHandled;
    }
}