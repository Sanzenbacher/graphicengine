//
//  IndexedBufferObject.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 15.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_IndexedBufferObject_h
#define macterrain_IndexedBufferObject_h

#include "RenderObject.h"
#include "SceneObjectProperties.h"
#include "TextureRenderObject.h"

class TextureDataBase;

namespace RendererOpenGL_2
{
    class InterleavedIndexBuffer;
    
    // This object represents a indexed vertex buffer object.
    // It offers setters for the used buffers, and the derived memberfunctions of RenderObject.
    class LandscapeRenderObject : public RenderObject
    {
    public:
        // Con-/Destructors
        LandscapeRenderObject(std::shared_ptr<SceneObjectProperties> description);
        ~LandscapeRenderObject();
        
        virtual void load(void);
        virtual void render(void);
        virtual void unload(void);
        
    protected:
        LandscapeRenderObject();
        
        // Constructs the triangle-net that will be used to render the terrain
        GLfloat getHeight(GLfloat currentXCoord, GLfloat currentZCoord);
        slm::vec3 getNormal(GLfloat xCoord, GLfloat zCoord, GLfloat normalizedHeight,
                            GLfloat triangleSize);
        void constructIndexBufferObject(void);
        
        std::shared_ptr<PropertyMapType> terrainParams;
        TextureDataBase* heightMap;
        InterleavedIndexBuffer* _terrainIndexBuffer;
    };
}
#endif
