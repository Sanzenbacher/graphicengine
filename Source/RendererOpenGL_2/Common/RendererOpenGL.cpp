/*
 *  RendererOpenGL.cpp
 *  macterrain
 *
 *  Created by Weiyun Lu on 1/8/12.
 *  Copyright 2012 Uni Stuttgart. All rights reserved.
 *
 */

#include "RendererOpenGL.h"

#include "RenderObject.h"
#include "CameraObject.h"

#include <algorithm>
#include <OpenGL/gl.h>

#pragma mark Constructors and destructors

namespace RendererOpenGL_2
{
    RendererOpenGL::RendererOpenGL(RenderObjectFactoryBase* renderObjectFactory, SceneManager* sceneManager) :
    RendererBase::RendererBase(renderObjectFactory, sceneManager)
    {
        
    }
    RendererOpenGL::RendererOpenGL() : RendererBase::RendererBase()
    {
    }
    RendererOpenGL::~RendererOpenGL()
    {
    }
    
    void RendererOpenGL::Render(void)
    {
        glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glDepthMask(GL_TRUE);
        glEnable(GL_TEXTURE_2D);
        
        // Create light components
        GLfloat ambientLight[] = { 0.8f, 0.2f, 0.2f, 1.0f };
        GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
        GLfloat specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };
        GLfloat position[] = { 100.0f, 100.0f, 100.0f, 0.0f };
        
        // Assign created components to GL_LIGHT0
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
        glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
        glLightfv(GL_LIGHT0, GL_POSITION, position);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        RendererBase::Render();
        
        glFlush();
    }
}