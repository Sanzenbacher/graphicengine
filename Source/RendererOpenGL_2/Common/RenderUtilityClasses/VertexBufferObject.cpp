//
//  VertexBufferObject.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 21.07.14.
//
//

#include "VertexBufferObject.h"

#include <OpenGL/gl.h>

namespace RendererOpenGL_2
{
    VertexBufferObject::VertexBufferObject(void) :
    _VBOID(0),
    _InterleavedData(nullptr),
    _InterleavedNumBytes(0),
    _VertexStride(0),
    _NormalDataOffset(-1),
    _TextureCoordOffset(-1)
    {
        
    }
    
    VertexBufferObject::~VertexBufferObject(void)
    {
        glDeleteBuffers(1, &_VBOID);
        
        if(_InterleavedData != nullptr)
        {
            delete [] (GLfloat*)_InterleavedData;
        }
    }
    
    void VertexBufferObject::setVertexData(GLvoid* vertexData, GLsizei numBytes, GLuint vertexStride)
    {
        _InterleavedData = vertexData;
        _InterleavedNumBytes = numBytes;
        _VertexStride = vertexStride;
        
        glGenBuffers(1, &_VBOID);
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        glBufferData(GL_ARRAY_BUFFER, _InterleavedNumBytes, _InterleavedData, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
    }
    
    void VertexBufferObject::setNormalDataOffset(GLint offset)
    {
        _NormalDataOffset = offset;
        
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        
    }
    
    void VertexBufferObject::setTextureCoordsDataOffset(GLint offset, GLint coordDim)
    {
        _TextureCoordOffset = offset;
        
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        
    }
    
    void VertexBufferObject::performRendering(void)
    {
        glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
        glBindBuffer(GL_ARRAY_BUFFER, _VBOID);
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, _VertexStride, (GLvoid*)0);
        
        if(_TextureCoordOffset != -1)
        {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(3*sizeof(GLfloat), GL_FLOAT, _VertexStride, (GLvoid*)_TextureCoordOffset);
        }
        if(_NormalDataOffset != -1)
        {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, _VertexStride, (GLvoid*)_NormalDataOffset);
        }
        
        glDrawArrays(GL_TRIANGLES, 0, _InterleavedNumBytes / (_VertexStride));
        glPopClientAttrib();
    }
}