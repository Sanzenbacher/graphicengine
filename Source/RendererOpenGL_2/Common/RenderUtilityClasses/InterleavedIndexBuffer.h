//
//  InterleavedIndexBuffer.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 19.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__InterleavedIndexBuffer__
#define __GraphicsPlayground_Mac__InterleavedIndexBuffer__

namespace RendererOpenGL_2
{
    class InterleavedIndexBuffer
    {
    public:
        InterleavedIndexBuffer(void);
        ~InterleavedIndexBuffer();
        
        void performRendering(void);
        
#pragma mark - Setup InterleavedIndexBuffer
        // Set the pointer to the vertex data
        // @param vertexData  pointer to vertices
        void setVertexData(GLvoid* vertexData, GLsizei numBytes, GLuint vertexStride);
        
        // Set the offset into the vertex array for the normals
        void setNormalDataOffset(GLint offset);
        
        // Set the offset into the vertex array for texture coordinates
        void setTextureCoordsDataOffset(GLint offset);
        
        // Set the pointer to the index data
        // @param indexData  pointer to indices
        void setIndexData(GLvoid* indexData, GLsizei numBytes);
    protected:
        GLuint _VertexStride;
        long  _NormalDataOffset;
        long  _TextureCoordOffset;
        
        GLvoid* _VertexData;
        GLsizei _VertexNumBytes;
        GLvoid* _IndexData;
        GLsizei _IndexNumBytes;
        
        // OpenGL internal data
        GLuint _VertexBufferID;
        GLuint _IndexBufferID;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__InterleavedIndexBuffer__) */
