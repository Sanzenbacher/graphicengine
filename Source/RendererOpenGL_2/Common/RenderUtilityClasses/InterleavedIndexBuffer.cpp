//
//  InterleavedIndexBuffer.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 19.07.14.
//
//

#include "InterleavedIndexBuffer.h"

#include <OpenGL/gl.h>

namespace RendererOpenGL_2
{
    InterleavedIndexBuffer::InterleavedIndexBuffer(void) :
    _VertexStride(0),
    _NormalDataOffset(-1),
    _TextureCoordOffset(-1),
    _VertexData(nullptr),
    _VertexNumBytes(0),
    _IndexData(nullptr),
    _VertexBufferID(-1),
    _IndexBufferID(-1)
    {
        
    }
    
    InterleavedIndexBuffer::~InterleavedIndexBuffer()
    {
        if(_VertexData != nullptr)
        {
            delete [] (GLfloat*)_VertexData;
        }
        if(_IndexData != nullptr)
        {
            delete [] (GLint*) _IndexData;
        }
        
        glDeleteBuffers(1, &_VertexBufferID);
        glDeleteBuffers(1, &_IndexBufferID);
    }
    
    // Set the pointer to the vertex data
    // @param vertexData  pointer to vertices
    void InterleavedIndexBuffer::setVertexData(GLvoid* vertexData, GLsizei numBytes, GLuint vertexStride)
    {
        _VertexData = vertexData;
        _VertexNumBytes = numBytes;
        _VertexStride = vertexStride;
        
        glGenBuffers(1, &_VertexBufferID);
        glBindBuffer(GL_ARRAY_BUFFER, _VertexBufferID);
        glBufferData(GL_ARRAY_BUFFER, _VertexNumBytes, _VertexData, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ARRAY_BUFFER, _VertexBufferID);
        
    }
    
    // Set the offset into the vertex array for the normals
    void InterleavedIndexBuffer::setNormalDataOffset(GLint offset)
    {
        _NormalDataOffset = offset;
        
        glBindBuffer(GL_ARRAY_BUFFER, _VertexBufferID);
        
    }
    
    void InterleavedIndexBuffer::setTextureCoordsDataOffset(GLint offset)
    {
        _TextureCoordOffset = offset;
        
        glBindBuffer(GL_ARRAY_BUFFER, _VertexBufferID);
    }
    
    // Set the pointer to the index data
    // @param indexData  pointer to indices
    void InterleavedIndexBuffer::setIndexData(GLvoid* indexData, GLsizei numBytes)
    {
        _IndexData = indexData;
        _IndexNumBytes = numBytes;
        
        glGenBuffers(1, &_IndexBufferID);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _IndexBufferID);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, _IndexNumBytes, _IndexData, GL_STATIC_DRAW);
    }
    
    void InterleavedIndexBuffer::performRendering(void)
    {
        glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
        glBindBuffer(GL_ARRAY_BUFFER, _VertexBufferID);
        if(_IndexBufferID != -1)
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _IndexBufferID);
        }
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, _VertexStride, 0);
        
        if(_TextureCoordOffset != -1)
        {
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(2, GL_FLOAT, _VertexStride, (GLvoid*)_TextureCoordOffset);
        }
        
        if(_NormalDataOffset != -1)
        {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, _VertexStride, (GLvoid*)_NormalDataOffset);
        }
        
        glDrawElements(GL_QUADS, (_IndexNumBytes/(GLsizei)sizeof(GLint)), GL_UNSIGNED_INT, 0);
        
        glPopClientAttrib();
    }
}