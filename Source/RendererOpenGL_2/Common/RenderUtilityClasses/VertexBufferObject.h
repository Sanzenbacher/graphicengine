//
//  VertexBufferObject.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 21.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__VertexBufferObject__
#define __GraphicsPlayground_Mac__VertexBufferObject__

namespace RendererOpenGL_2
{
    class VertexBufferObject
    {
    public:
        VertexBufferObject(void);
        ~VertexBufferObject(void);
        
#pragma mark - Setup VertexBufferObject
        // Set the pointer to the vertex data
        // @param vertexData  pointer to vertices
        void setVertexData(GLvoid* vertexData, GLsizei numBytes, GLuint vertexStride);
        
        // Set the offset into the vertex array for the normals
        void setNormalDataOffset(GLint offset);
        
        // Set the offset into the vertex array for texture coordinates
        void setTextureCoordsDataOffset(GLint offset, GLint coordDim);
        
        void performRendering(void);
    protected:
        GLuint _VBOID;
        
        GLvoid* _InterleavedData;
        GLsizei _InterleavedNumBytes;
        
        GLuint _VertexStride;
        long  _NormalDataOffset;
        long  _TextureCoordOffset;
    };
}

#endif /* defined(__GraphicsPlayground_Mac__VertexBufferObject__) */
