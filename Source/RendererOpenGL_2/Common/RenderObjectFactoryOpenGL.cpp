//
//  RenderObjectFactoryOpenGL.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 22.06.14.
//
//

#include "RenderObjectFactoryOpenGL.h"

#include "RendererOpenGL_2/Common/RenderObjects/LandscapeRenderObject.h"
#include "RendererOpenGL_2/Common/RenderObjects/TriangleObject.h"
#include "RendererOpenGL_2/Common/RenderObjects/CameraObject.h"
#include "RendererOpenGL_2/Common/RenderObjects/StaticMeshRenderObject.h"

class SceneObjectProperties;

namespace RendererOpenGL_2
{
    RenderObject* RenderObjectFactoryOpenGL::CreateRenderObject(std::shared_ptr<SceneObjectProperties> properties)
    {
        RenderObject* renderObject = NULL;
        PresentationType presentationType = ttl::var::get<PresentationType>(properties->presentation[PropertyPresentationType]);
        switch (presentationType) {
            case PresentationType::PT_TYPE_TRIANGLE:
                renderObject = new TriangleObj(properties);
                break;
            case PresentationType::PT_TYPE_STATIC_HIGHTMAP:
                renderObject = new LandscapeRenderObject(properties);
                break;
            case PresentationType::PT_TYPE_CAMERA:
                renderObject = new CameraObject(properties);
                break;
            case PresentationType::PT_TYPE_STATIC_MESH:
                renderObject = new StaticMeshRenderObject(properties);
                break;
            default:
                assert(false); // RenderObject type not defined
        }
        assert(renderObject != NULL);
        InitializeRenderObject(renderObject, properties);
        return renderObject;
    }
}
