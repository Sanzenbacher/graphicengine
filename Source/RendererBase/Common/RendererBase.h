//
//  RendererBase.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 22.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__RendererBase__
#define __GraphicsPlayground_Mac__RendererBase__

#include <iostream>
#include <vector>

class Identifier;
class SceneManager;
class ScObj_SimpleCamera;
class RenderObject;
class RenderObjectFactoryBase;

class RendererBase
{
public:
    RendererBase(RenderObjectFactoryBase* renderObjectFactory, SceneManager* sceneManager);
    virtual ~RendererBase();
    
    virtual void Render(void);
    
    void registerCamera(ScObj_SimpleCamera* cameraObject);
protected:
    typedef std::vector<RenderObject*> RenderObjectVector;
    RenderObjectVector _renderObjects;
    RenderObjectFactoryBase* _renderObjectFactory;
    SceneManager* _sceneManager;
    RenderObject* _activeCamera;
    
    RendererBase();
};

#endif /* defined(__GraphicsPlayground_Mac__RendererBase__) */
