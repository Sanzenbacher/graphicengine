//
//  RendererBase.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 22.06.14.
//
//

#include "RendererBase.h"

#include "RendererBase/Common/RenderObject.h"
#include "RendererBase/Common/RenderObjectFactoryBase.h"

#include "Scene/Common/SceneManager.h"
#include "Scene/Common/SceneObjBase.h"
#include "Scene/Common/SceneObjectProperties.h"
#include "Scene/Common/ScObj_SimpleCamera.h"

RendererBase::RendererBase(RenderObjectFactoryBase* renderObjectFactory, SceneManager* sceneManager)
{
    _renderObjectFactory = renderObjectFactory;
    _sceneManager = sceneManager;
    _activeCamera = NULL;
}

RendererBase::RendererBase()
{
    assert(false); // This constructor should never be called !
}

RendererBase::~RendererBase()
{
    if(_renderObjectFactory)
    {
        delete _renderObjectFactory;
    }
 
    if(_sceneManager)
    {
        delete _sceneManager;
    }
    
    if(_activeCamera)
    {
        delete _activeCamera;
    }
}

void RendererBase::Render(void)
{
    SceneObjBaseVector newVisibleObjects = _sceneManager->queryNewVisibleObjects();
    for(SceneObjBaseVector::iterator it = newVisibleObjects.begin();
        it != newVisibleObjects.end(); it++)
    {
        std::shared_ptr<SceneObjectProperties> description = (*it)->properties();
        RenderObject* newRenderObject = _renderObjectFactory->CreateRenderObject( description );
        newRenderObject->load();
        _renderObjects.push_back(newRenderObject);
    }
    
    SceneObjBaseVector obsoleteObjects = _sceneManager->queryObsoleteObjects();
    for(SceneObjBaseVector::iterator obsoleteIt = obsoleteObjects.begin();
        obsoleteIt != obsoleteObjects.end(); obsoleteIt++)
    {
        for(RenderObjectVector::iterator roIt = _renderObjects.begin();
            roIt != _renderObjects.end(); roIt++)
        {
            if((*roIt)->identifier() == (*obsoleteIt)->properties()->identifier)
            {
                (*roIt)->unload();
                _renderObjects.erase(roIt);
                break;
            }
        }
    }
    
    _activeCamera->render();
    
    for(RenderObjectVector::iterator roIt = _renderObjects.begin();
       roIt != _renderObjects.end(); roIt++)
    {
        (*roIt)->render();
    }
}

void RendererBase::registerCamera(ScObj_SimpleCamera* cameraObject)
{
    if(_activeCamera)
    {
        _activeCamera->unload();
        delete _activeCamera;
    }
    
    _activeCamera = _renderObjectFactory->CreateRenderObject(cameraObject->properties());
    if(_activeCamera)
    {
        _activeCamera->load();
    }
}