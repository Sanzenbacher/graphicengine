//
//  RenderObjectFactoryBase.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 22.06.14.
//
//

#include "RenderObjectFactoryBase.h"

#include "RenderObject.h"

#include "SceneObjectProperties.h"

class Manipulator;

RenderObjectFactoryBase::~RenderObjectFactoryBase()
{
    
}

void RenderObjectFactoryBase::InitializeRenderObject(RenderObject* renderObject, std::shared_ptr<SceneObjectProperties> properties)
{
    try {
        Manipulator* manipulator = ttl::var::get<Manipulator*>(properties->presentation[PropertyPresentationManipulator]);
        manipulator->setManipulationHandler(renderObject);
    } catch (ttl::var::exception) {
    }    
}