/*
 *  RenderObject.cpp
 *  macterrain
 *
 *  Created by Weiyun Lu on 1/8/12.
 *  Copyright 2012 Uni Stuttgart. All rights reserved.
 *
 */

#include "RenderObject.h"

// Con-/Destructor
RenderObject::RenderObject(const Identifier& identifier) :
    _identifier(identifier)
{
    _isLoaded = false;
}
RenderObject::RenderObject::~RenderObject()
{
    unload();
}

// Upload data to gpu
void RenderObject::load(void)
{
    for(int i=0; i<rosBeforeSelf.size(); i++)
        rosBeforeSelf[i]->load();
    
    _isLoaded = true;
}

// Draw uploaded data with gpu
void RenderObject::render(void)
{
    for(int i=0; i<rosBeforeSelf.size(); i++)
        rosBeforeSelf[i]->render();
}

// Unload data from gpu
void RenderObject::unload(void)
{
    for(int i=0; i<rosBeforeSelf.size(); i++)
        rosBeforeSelf[i]->unload();
    
    _isLoaded = false;
}

bool RenderObject::isLoaded(void)
{
    return _isLoaded;
}

Identifier RenderObject::identifier(void)
{
    return _identifier;
}

void RenderObject::addRenderObject(RenderObject* ro, RenderObjectOrder order)
{
    switch(order)
    {
        case RO_ORDER_BEFORE_SELF:
            rosBeforeSelf.push_back(ro);
            break;
        /*case RO_ORDER_AFTER_SELF:
            rosAfterSelf.push_back(ro);
            break;*/
        default:
            assert(false);
    }
}

#pragma region - ManipulationHandler

bool RenderObject::manipulatePropertyWithName(std::wstring* const propertyName, void* const propertyValue)
{
    // RenderObject has no property to handle
    return false;
}