/*
 *  RenderObject.h
 *  macterrain
 *
 *  Created by Weiyun Lu on 1/8/12.
 *  Copyright 2012 Uni Stuttgart. All rights reserved.
 *
 */

#pragma once

#include <vector>

#include "Scene/Common/SceneObjectProperties.h"
#include "Scene/Common/ManipulationHandler.h"
#include "Scene/Common/Identifier.h"

// Forward declarations
class RenderObject;

// Type definitions
typedef unsigned int MaterialID;
enum RenderObjectOrder
{
    RO_ORDER_BEFORE_SELF,
    //RO_ORDER_AFTER_SELF
};

// This class is used by the renderer to upload, unload and render 3d model data.
class RenderObject : public ManipulationHandler
{
public:
    // Con-/Destructor
    RenderObject(const Identifier& identifier);
    virtual ~RenderObject();
    
    // Upload data to gpu
    virtual void load(void);
    
    // Draw uploaded data with gpu
    virtual void render(void);
    
    // Unload data from gpu
    virtual void unload(void);
    
    // Return the upload state
    bool isLoaded(void);
    Identifier identifier(void);
    
    // This function allows to combine Render Objects with each other
    void addRenderObject(RenderObject* ro, RenderObjectOrder order);
    
    // Override ManipulationHandler
    virtual bool manipulatePropertyWithName(std::wstring* const propertyName, void* const propertyValue);
private:
    bool _isLoaded;
    
    std::vector<RenderObject*> rosBeforeSelf;
    //std::vector<RenderObject*> rosAfterSelf;
    
    Identifier _identifier;
};