//
//  RenderObjectFactoryBase.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 21.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__RenderObjectFactoryBase__
#define __GraphicsPlayground_Mac__RenderObjectFactoryBase__

#include <iostream>

#include "Scene/Common/SceneObjBase.h"

class RenderObject;
class SceneObjectProperties;

class RenderObjectFactoryBase
{
public:
    virtual RenderObject* CreateRenderObject(std::shared_ptr<SceneObjectProperties> properties) = 0;
    virtual ~RenderObjectFactoryBase();
protected:
    void InitializeRenderObject(RenderObject* renderObject, std::shared_ptr<SceneObjectProperties> properties);
};

#endif /* defined(__GraphicsPlayground_Mac__RenderObjectFactoryBase__) */
