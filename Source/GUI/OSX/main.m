//
//  main.m
//  macterrain
//
//  Created by Weiyun Lu on 1/6/12.
//  Copyright Uni Stuttgart 2012. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
