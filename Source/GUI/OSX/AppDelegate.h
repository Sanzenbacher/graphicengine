//
//  AppDelegate.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 11.01.15.
//
//

#import <Foundation/Foundation.h>

@interface AppDelegate : NSObject<NSApplicationDelegate>

@end
