//
//  MainGLVIew.h
//  macterrain
//
//  Created by Weiyun Lu on 1/8/12.
//  Copyright 2012 Uni Stuttgart. All rights reserved.
//

#import <QuartzCore/CVDisplayLink.h>

#define USE_OPENGL_3

#if defined(USE_OPENGL_3)
namespace RendererOpenGL_3
{
    class RendererOpenGL;
}
#elif defined(USE_OPENGL_2)
namespace RendererOpenGL_2
{
    class RendererOpenGL;
}
#endif
class SceneManager;
class ScObj_SimpleCamera;

@interface MainGLView : NSOpenGLView {
#if defined(USE_OPENGL_3)
    RendererOpenGL_3::RendererOpenGL* m_renderer;
#elif defined(USE_OPENGL_2)
    RendererOpenGL_2::RendererOpenGL* m_renderer;
#endif
    SceneManager*   m_sceneManager;
    ScObj_SimpleCamera* m_egoCamera;
    
@private    
    CVDisplayLinkRef displayLink; //display link for managing rendering thread
    
    bool mouseActive;
}

- (id)initWithFrame:(NSRect)frameRect;

// Functions that are called by notifications
- (void) sizeChanged:(NSNotification*)notification;

- (void)update;

-(void)keyUp:(NSEvent*)event;

- (CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime;

@end
