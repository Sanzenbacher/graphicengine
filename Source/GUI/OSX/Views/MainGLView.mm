//
//  MainGLVIew.m
//  macterrain
//
//  Created by Weiyun Lu on 1/8/12.
//  Copyright 2012 Uni Stuttgart. All rights reserved.
//

#import "MainGLView.h"

#include<OpenGL/gl.h>
#include<OpenGL/glu.h>
#include <OpenGL/OpenGL.h>

#if defined(USE_OPENGL_3)
#include "RendererOpenGL_3/Common/RendererOpenGL.h"
#include "RendererOpenGL_3/Common/RenderObjectFactoryOpenGL.h"
#elif defined(USE_OPENGL_2)
#include "RendererOpenGL_2/Common/RendererOpenGL.h"
#include "RendererOpenGL_2/Common/RenderObjectFactoryOpenGL.h"
#endif
#include "Scene/Common/SceneManager.h"
#include "Scene/Common/SceneObjBase.h"
#include "Scene/Common/SceneObjectProperties.h"
#include "Scene/Common/ScObj_SimpleCamera.h"
#include "Scene/Common/ScObj_SimpleTerrain.h"
#include "Scene/Common/ScObj_Triangle.h"
#include "Scene/Common/ScObj_StaticMesh.h"

#pragma mark Forward declarations
static CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime,
                                      CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* displayLinkContext);

@implementation MainGLView

#pragma mark NSView methods
- (instancetype)initWithCoder:(NSCoder *)coder
{
    NSOpenGLPixelFormatAttribute attrs[] =
    {
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 32,
#if defined(USE_OPENGL_3)
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
#elif defined(USE_OPENGL_2)
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersionLegacy,
#endif
        0
    };
        
    NSOpenGLPixelFormat* format = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    self = [super initWithCoder:coder];
    self.pixelFormat = format;
    if (self == nil)
    {
        NSLog(@"Initialization of pixel format failed !!!");
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    NSOpenGLPixelFormatAttribute attrs[] =
    {
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 32,
#if defined(USE_OPENGL_3)
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
#elif defined(USE_OPENGL_2)
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersionLegacy,
#endif
        0
    };
    
    NSOpenGLPixelFormat* format = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    self = [super initWithFrame:frameRect pixelFormat:format];
    if (self != nil)
    {
        NSLog(@"Initialization of pixel format failed !!!");
    }
    return self;
}

- (void) awakeFromNib
{    
    [self becomeFirstResponder];
    [self.window setAcceptsMouseMovedEvents:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sizeChanged:)
                                                 name:NSViewGlobalFrameDidChangeNotification
                                               object:self];

    NSOpenGLPixelFormatAttribute attrs[] =
    {
//        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 32,
#if defined(USE_OPENGL_3)
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
#elif defined(USE_OPENGL_2)
        NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersionLegacy,
#endif
        0
    };

    NSOpenGLPixelFormat* format = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    [self setPixelFormat:format];
}

- (void) sizeChanged:(NSNotification*)notification
{
    [self update];
}

- (void)update
{
    [super update];
    
    if ([[self openGLContext] view] == self)
    {
        [[self openGLContext] update];
    }
    
    CGRect newSize = [self convertRectToBacking:[self bounds]];;
    
    //GLfloat width = newSize.size.width - newSize.origin.x;
    //GLfloat height = newSize.size.height - newSize.origin.y;
    
    glViewport(0, 0, newSize.size.width, newSize.size.height);
    
#if defined(USE_OPENGL_2)
    GLdouble aspectRatio = (GLdouble)(newSize.size.width) / (GLdouble)(newSize.size.height);
    GLdouble nearClipPlane = 0.1;
    GLdouble farClipPlane = 10000.0;
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    slm::mat4 perspectiveTransformation = slm::perspectiveFovRH(70.0, aspectRatio, nearClipPlane, farClipPlane);
    glMultMatrixf(perspectiveTransformation.begin());
#endif
//    gluPerspective(70.0, aspectRatio, nearClipPlane, farClipPlane);
}

- (void)prepareOpenGL
{
    [super prepareOpenGL];
    
    [[self openGLContext] makeCurrentContext];
    
    // Initialize camera
    {
        SceneObjectProperties properties;
        slm::vec3 position = slm::vec3(0.5, 0.0, 0.0);
        position = slm::vec3(0);
        slm::vec3 target   = slm::normalize(slm::vec3(0.0, 0.0, 1.0));
        slm::vec3 up       = slm::vec3(0., 1., 0.);
        slm::mat4 transformation = slm::lookAtRH(position, target, up);
        properties.presentation[PropertyPresentationTransformation] = transformation;
        slm::mat4 perspectiveTransformation = slm::perspectiveFovRH(70.0, 1024.0/768.0, 0.1, 10000.0);
        properties.presentation[ScObj_SimpleCamera::PropertyPresentationPerspectiveTransformation] = perspectiveTransformation;
        m_egoCamera = new ScObj_SimpleCamera(properties);
    }
    
    // Initialize scene
    m_sceneManager = new SceneManager();
    m_sceneManager->registerCamera(m_egoCamera);
    
    // Landscape
    {
        SceneObjectProperties properties;
        properties.presentation[ScObj_SimpleTerrain::PropertyPresentationTotalDepth] = 32.0f;
        properties.presentation[ScObj_SimpleTerrain::PropertyPresentationTotalWidth] = 32.0f;
        properties.presentation[ScObj_SimpleTerrain::PropertyPresentationTotalHeight] = 16.0f;
        properties.presentation[ScObj_SimpleTerrain::PropertyPresentationDetailFactor] = 2.0f;
        properties.presentation[ScObj_SimpleTerrain::PropertyPresentationHeightMapFilename] = std::wstring(L"Kbsd_Heightmap_Example.jpg");
        std::vector<std::wstring> landscapeTextures;
        landscapeTextures.push_back(std::wstring(L"white.jpg"));
        properties.presentation[PropertyPresentationTextures] = landscapeTextures;
        ScObj_SimpleTerrain* terrainObj = new ScObj_SimpleTerrain(properties);
        m_sceneManager->registerObject(terrainObj);
    }

    // Triangle
//    {
//        SceneObjectProperties properties;
//        std::vector<std::wstring> textures;
//        textures.push_back(L"heightmap-withFoggyFilter.jpg");
//        properties.presentation[PropertyPresentationTextures] = textures;
//        properties.presentation[PropertyPresentationTransformation] = slm::translation(slm::vec3(0.0, 0.0, 0.0));
//        ScObj_Triangle* triangleObj = new ScObj_Triangle(properties);
//        m_sceneManager->registerObject(triangleObj);
//    }
    
    // Static mesh
    {
        SceneObjectProperties properties;
        properties.presentation[PropertyPresentationTransformation] = slm::translation(slm::vec3(0.0, 0.0, 0.0));
        properties.presentation[ScObj_StaticMesh::PropertyPresentationStaticMeshFilename] = std::string("untitled.obj");
        ScObj_StaticMesh* staticMesh = new ScObj_StaticMesh(properties);
        m_sceneManager->registerObject(staticMesh);
    }
    
    // Initialization renderer
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
#if defined(USE_OPENGL_3)
    m_renderer = new RendererOpenGL_3::RendererOpenGL(new RendererOpenGL_3::RenderObjectFactoryOpenGL(), m_sceneManager);
#elif defined(USE_OPENGL_2)
    m_renderer = new RendererOpenGL_2::RendererOpenGL(new RendererOpenGL_2::RenderObjectFactoryOpenGL(), m_sceneManager);
#endif
    m_renderer->registerCamera(m_egoCamera);
    
    // Synchronize buffer swaps with vertical refresh rate
    GLint swapInt = 1;
//    NSOpenGLProfileVersion3_2Core
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // Create a display link capable of being used with all active displays
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    
    // Set the renderer output callback function
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
    
    // Set the display link for the current renderer
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    
    // Activate the display link
    CVDisplayLinkStart(displayLink);
    
    // setup OpenGL
    [self update];

    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)reshape
{
    [super reshape];
}

- (void)dealloc
{
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    CVDisplayLinkRelease(displayLink);
    
    // Free cpp members
    delete m_sceneManager;
    delete m_renderer;
    
    // Cleanup opengl stuff
    [self clearGLContext];
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    
    [super dealloc];
}

- (void)drawRect:(NSRect)rect
{
    if (!CVDisplayLinkIsRunning(displayLink))
    {
//        m_renderer->Render();
//        [[self openGLContext] flushBuffer];
    }
}

#pragma mark Keyboard input
- (BOOL)acceptsFirstResponder 
{
    return YES;
}

-(void)keyUp:(NSEvent*)event
{
    //NSLog(@"Key released: %@", event);
}

-(void)keyDown:(NSEvent*)event
{   
    // Faster movement when shift is pressed
    float movementValue = 0.05f;
    if([event modifierFlags] & NSShiftKeyMask)
        movementValue = 5;
    
    // I added these based on the addition to your question :)
    switch( [event keyCode] ) {
        case 126:       // up arrow
            m_egoCamera->moveForward(movementValue);
            break;
        case 125:       // down arrow
            m_egoCamera->moveBackward(movementValue);
            break;
        case 124:       // right arrow
            m_egoCamera->moveRight(movementValue);
            break;
        case 123:       // left arrow
            m_egoCamera->moveLeft(movementValue);
            break;
        default:
            NSLog(@"Key pressed: %@", event);
            break;
    }
    //[self needsDisplay];
}

#pragma mark Mouse input
-(void)mouseDown:(NSEvent *)theEvent
{
    mouseActive = true;
}
-(void)mouseUp:(NSEvent *)theEvent
{
    mouseActive = false;
}
-(void)mouseDragged:(NSEvent *)theEvent
{
    //NSLog(@"%@",theEvent);
    m_egoCamera->rotateX((float)-theEvent.deltaY/100.0f);
    m_egoCamera->rotateY((float)theEvent.deltaX/100.0f);
}


#pragma mark Callbacks
// This is the renderer output callback function
static CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime,
                                      CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* displayLinkContext)
{
    CVReturn result = [(MainGLView*)displayLinkContext getFrameForTime:outputTime];
    return result;
}

- (CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime
{   
    [[self openGLContext] makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

    // Add your drawing codes here
    m_renderer->Render();
    [[self openGLContext] flushBuffer];
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

    return kCVReturnSuccess;
}


@end
