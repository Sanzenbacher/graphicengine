//
//  AppDelegate.m
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 11.01.15.
//
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

@end
