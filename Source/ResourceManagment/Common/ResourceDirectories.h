//
//  ResourceDirectories.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 15.08.14.
//
//

#ifndef __GraphicsPlayground_Mac__ResourceDirectories__
#define __GraphicsPlayground_Mac__ResourceDirectories__

class ResourceDirectories
{
public:
    static ResourceDirectories* getInstance(void);
    
    std::string shaderDirectory(void);
    
protected:
    ResourceDirectories(void);
    
    static ResourceDirectories* m_instance;
    
    std::string m_shaderDirectory;
};

#endif /* defined(__GraphicsPlayground_Mac__ResourceDirectories__) */
