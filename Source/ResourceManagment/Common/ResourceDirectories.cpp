//
//  ResourceDirectories.mm
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 15.08.14.
//
//

#include "ResourceDirectories.h"

#if defined(__MACH__)
    #include "ResourceManagment/OSX/ResourceDirectoriesOSX.h"
#endif

ResourceDirectories* ResourceDirectories::m_instance = nullptr;

ResourceDirectories* ResourceDirectories::getInstance(void)
{
    if(m_instance == nullptr)
    {
#if defined(__MACH__)
        m_instance = new ResourceDirectoriesOSX();
#else
        m_instance = new ResourceDirectories();
#endif
    }
    return m_instance;
}

ResourceDirectories::ResourceDirectories(void)
{
}

std::string ResourceDirectories::shaderDirectory(void)
{
    return m_shaderDirectory;
}
