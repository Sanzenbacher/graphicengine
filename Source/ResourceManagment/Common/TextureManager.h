//
//  ResourceManagerBase.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 10.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_TextureManager_h
#define macterrain_TextureManager_h

#include "string"

// Forward declarations
class TextureManager;
class TextureDataBase;

// This class can load / unload textures
class TextureManager
{
public:
    // Desturctor
    virtual ~TextureManager();
    
    // Singleton access
    static TextureManager* getInstance(void);
    
    // Load a texture, when loaded return it
    TextureDataBase* loadTexture(std::wstring& texturePath);
    
    // Unload texture
    void        unloadTexture(std::wstring& texturePath);
private:
    // Private constructor
    TextureManager();
    
    static TextureManager* s_instance;
};

#endif
