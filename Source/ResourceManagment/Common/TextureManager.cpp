//
//  ResourceManagerBase.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 10.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "TextureManager.h"

#include "TextureDataBase.h"

#if defined(__MACH__)
    #include "ResourceManagment/OSX/TextureDataOSX.h"
#endif

TextureManager* TextureManager::s_instance = NULL;

TextureManager::~TextureManager()
{
}

TextureManager* TextureManager::getInstance(void)
{
    if(s_instance == NULL)
        s_instance = new TextureManager();
    
    return s_instance;
}

TextureDataBase* TextureManager::loadTexture(std::wstring& textureFilename)
{
    TextureDataBase* textureData = NULL;
#if defined(__MACH__)
    textureData = new TextureDataOSX(textureFilename);
#endif
    assert(textureData);
    return textureData;
}
void TextureManager::unloadTexture(std::wstring& texturePath)
{
    // Implement this !
    assert(false);
}

// Private constructor
TextureManager::TextureManager()
{
}