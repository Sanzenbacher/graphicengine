//
//  TextureDataBase.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 09.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__TextureDataBase__
#define __GraphicsPlayground_Mac__TextureDataBase__

#include <iostream>

struct RgbaColor
{
public:
    RgbaColor(),
    RgbaColor(slm::vec3 colorVec);
    
    float red;
    float green;
    float blue;
    float alpha;
    
    slm::vec3 getVec3();
};

class TextureDataBase
{
public:
    TextureDataBase(std::wstring& fullFilename);
    virtual ~TextureDataBase();
    
    // Return the color of the specified pixel using nearest neighbour
    // @param x  Normalized ranging between 0 and 1
    // @param y  Normalized ranging between 0 and 1
    virtual RgbaColor getPixelColorFloat(float x, float y) = 0;
    virtual RgbaColor getPixelColorInt(long x, long y) = 0;
    
    // Return the color of the specified pixel using bilinear interpolation
    // @param x  Normalized ranging between 0 and 1
    // @param y  Normalized ranging between 0 and 1
    virtual RgbaColor getPixelInterpolatedColor(float x, float y);
    
    std::wstring    fullFilename;
    int32_t         width;
    int32_t         height;
    bool            hasAlpha;
    uint32_t        format;
    unsigned char*  bitmapData;
private:
    TextureDataBase();
};

#endif /* defined(__GraphicsPlayground_Mac__TextureDataBase__) */
