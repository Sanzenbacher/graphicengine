//
//  TextureDataBase.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 09.07.14.
//
//

#include "TextureDataBase.h"

#pragma mark - RgbaColor
RgbaColor::RgbaColor() : red(0.0f), green(0.0f), blue(0.0f), alpha(0.0f)
{
    
}

RgbaColor::RgbaColor(slm::vec3 colorVec)
{
    red = colorVec.x;
    green = colorVec.y;
    blue = colorVec.z;
    alpha = 1.0f;
}
slm::vec3 RgbaColor::getVec3()
{
    return slm::vec3(red, green, blue);
}

#pragma mark - TextureDataBase

TextureDataBase::TextureDataBase(std::wstring& fullFilename) :
    fullFilename(std::wstring()),
    width(0),
    height(0),
    hasAlpha(false),
    format(0),
    bitmapData(nullptr)
{
}

// Constructor shouldn't be used !
TextureDataBase::TextureDataBase()
{
    assert(false);
}

TextureDataBase::~TextureDataBase()
{
    
}

RgbaColor TextureDataBase::getPixelInterpolatedColor(float x, float y)
{
    float scaledX = x*width;
    float scaledY = y*height;
    uint32_t minX = (uint32_t)floor(scaledX);
    uint32_t minY = (uint32_t)floor(scaledY);
    float weightX = scaledX - (float)minX;
    float weightY = scaledY - (float)minY;
    float weightXInv = 1.0f-weightX;
    float weigthYInv = 1.0f-weightY;
    
    RgbaColor finalColor( weightXInv*weigthYInv*getPixelColorInt(minX, minY).getVec3() +
                         weightXInv*weightY*getPixelColorInt(minX, minY+1).getVec3() +
                         weightX*weigthYInv*getPixelColorInt(minX+1, minY).getVec3() +
                         weightX*weightY*getPixelColorInt(minX+1, minY+1).getVec3());
    return finalColor;
}