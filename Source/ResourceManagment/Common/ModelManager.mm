//
//  ModelManager.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 11.07.14.
//
//

#include "ModelManager.h"
#include "Models/ObjLoader.h"

ModelManager* ModelManager::s_instance = NULL;

ModelManager::~ModelManager()
{
}

ModelManager* ModelManager::getInstance(void)
{
    if(s_instance == NULL)
        s_instance = new ModelManager();
    
    return s_instance;
}

ModelLoader::ModelData* ModelManager::loadModel(std::string& modelFilename)
{
    ModelLoader::ModelData* modelData = NULL;
    
    // Get file ending, so we can decide how to load it
    std::string fileEnding = modelFilename.substr(modelFilename.find_last_of("."));
    if(fileEnding.compare(".obj") == 0)
    {
        NSString *objcFilename = [NSString stringWithUTF8String: modelFilename.c_str()];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:objcFilename ofType:@""];
        std::string *path = new std::string([filePath UTF8String]);
        
        modelData = ModelLoader::loadModel(*path);
    }
    
    assert(modelData);
    return modelData;
}
void ModelManager::unloadModel(std::string& texturePath)
{
    // Implement this !
    assert(false);
}

// Private constructor
ModelManager::ModelManager()
{
}
