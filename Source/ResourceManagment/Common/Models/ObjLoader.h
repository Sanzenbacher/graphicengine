//
//  AseLoader.h
//  AseLoader
//
//  Created by Sascha Sanzenbacher on 12.03.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

#ifndef ObjLoader_ObjLoader_h
#define ObjLoader_ObjLoader_h

#include <string>

namespace ModelLoader
{
    // Predefinitions
    class ModelData;
    
    ModelData* loadModel(std::string filename);
}

#endif
