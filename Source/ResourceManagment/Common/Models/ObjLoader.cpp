//
//  AseLoader.cpp
//  AseLoader
//
//  Created by Sascha Sanzenbacher on 12.03.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

#include "ObjLoader.h"
#include "ModelData.h"

using namespace ModelLoader;

#pragma variables
std::wifstream file;
std::wstring currentLine;
ModelData* model;
bool gotNormals;
bool gotTexCoords;

#pragma mark - Function forward definitions
std::wstring getNextLine(void);
void parseTrippleFloat(float& x, float& y, float& z);
void parseDoubleFloat(float& x, float& y);
void parseMaterialFilename(void);
void parseObjectName(void);
void parseComment(void);
void parseVertex(void);
void parseNormal(void);
void parseTexCoords(void);
void parseUsedMaterialName(void);
void parseFace(void);

std::wstring getNextLine(void)
{
    std::wstring nextLine;
    
    std::getline(file, nextLine, L'\n');
//    std::wcout << "before whitespace cleanup: " << nextLine.c_str() << std::endl;
    
    // Remove disturbing whitespaces
    int stringPos = 0;
    while(stringPos < nextLine.length())
    {
        wchar_t currentChar = nextLine[stringPos];
        if(currentChar == '\t')
            nextLine = nextLine.replace(stringPos, 1, L" ");
        else if(stringPos == 0 && currentChar == ' ')
            nextLine = nextLine.erase(stringPos, 1);
        else
            stringPos++;
    }
    
//    std::wcout << " after whitespace cleanup: " << nextLine.c_str() << std::endl;
    return nextLine;
}
void parseTrippleFloat(float& x, float& y, float& z)
{
    std::wistringstream is( currentLine );
    is >> x;
    if( is.fail() )
        std::wcout << L"Couldn't read vertex x-coord" << std::endl;
    is >> y;
    if( is.fail() )
        std::wcout << L"Couldn't read vertex y-coord" << std::endl;
    is >> z;
    if( is.fail() )
        std::wcout << L"Couldn't read vertex z-coord" << std::endl;
}
void parseDoubleFloat(float& x, float& y)
{
    std::wistringstream is( currentLine );
    is >> x;
    if( is.fail() )
        std::wcout << L"Couldn't read vertex x-coord" << std::endl;
    is >> y;
    if( is.fail() )
        std::wcout << L"Couldn't read vertex y-coord" << std::endl;
}

// Parse categories
void parseMaterialFilename(void)
{
}
void parseObjectName(void)
{
    size_t firstCharOfName = currentLine.find_first_not_of(L' ', 1);
    
    model->Name = currentLine.substr(firstCharOfName);
    
//    std::wcout << " Name: " << model->Name << std::endl;
}
void parseComment(void)
{
}
void parseVertex(void)
{
    currentLine = currentLine.substr(currentLine.find_first_not_of(' ', 2));
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    parseTrippleFloat(x, y, z);
    
    model->Vertices.push_back(x);
    model->Vertices.push_back(y);
    model->Vertices.push_back(z);
    
//    std::wcout << " Vertex: " << x << ", "<< y << ", " << z << std::endl;
}
void parseNormal(void)
{
    currentLine = currentLine.substr(currentLine.find_first_not_of(' ', 2));
    float nx = 0.0f;
    float ny = 0.0f;
    float nz = 0.0f;
    parseTrippleFloat(nx, ny, nz);
    
    model->Normals.push_back(nx);
    model->Normals.push_back(ny);
    model->Normals.push_back(nz);
    
//    std::wcout << " Normal: " << nx << ", "<< ny << ", " << nz << std::endl;
}
void parseTexCoords(void)
{
    short UVDim = model->UVCoordsDimension;
    
    currentLine = currentLine.substr(currentLine.find_first_not_of(' ', 2));
    float tu = 0.0f;
    float tv = 0.0f;
    float tw = 0.0f;
    
    if(UVDim == 3)
    {
        parseTrippleFloat(tu, tv, tw);
        model->UVCoords.push_back(tu);
        model->UVCoords.push_back(tv);
        model->UVCoords.push_back(tw);
    }
    else if(UVDim == 2)
    {
        parseDoubleFloat(tu, tv);
        model->UVCoords.push_back(tu);
        model->UVCoords.push_back(tv);
    }
    else if(UVDim == -1)
    {
        std::wistringstream is( currentLine );
        is >> tu;
        if( is.fail() )
            std::wcout << L"Couldn't read vertex x-coord" << std::endl;
        is >> tv;
        if( is.fail() )
            std::wcout << L"Couldn't read vertex y-coord" << std::endl;
        // We we cannot read the third coordinate, we just got two !
        is >> tw;
        if( is.fail() )
            UVDim = 2;
        else
            UVDim = 3;
        model->UVCoordsDimension = UVDim;
    }
    
//    if(UVDim == 3)
//        std::wcout << " TexCoords: " << tu << ", "<< tv << ", " << tw << std::endl;
//    else if(UVDim == 2)
//        std::wcout << " TexCoords: " << tu << ", "<< tv << ", " << std::endl;
}
void parseUsedMaterialName(void)
{
}
void parseFace(void)
{
    
    // cut the textline into three pieces since we just support triangles
    currentLine = currentLine.substr(currentLine.find_first_not_of(L' ', 1));
    
    std::wstring faceStrings[3];
    size_t currentChar = -1;
    size_t nextChar = currentLine.find_first_of(L' ', 0);
    for( int i=0; i<3; i++)
    {
        faceStrings[i] = currentLine.substr(currentChar+1, nextChar - currentChar - 1);
        currentChar = nextChar;
        nextChar = currentLine.find_first_of(L' ', currentChar+1);
        if(nextChar == -1)
            nextChar = currentLine.length();
    }
    
    // Three Vertices...
    Triangle newTriangle;
    for( int i=0; i<3; i++)
    {        
//        std::wcout << "Face " << i << " string: " << faceStrings[i] << std::endl;
        int faceIndices[3];
        std::wistringstream is( faceStrings[i] );

        // ... contain up to three indices (vertex, texCoord and normal)
        for( int j=0; j<3; j++)
        {
            wchar_t seperator = ' ';
            is >> faceIndices[j];
            if(is.fail())
            {
                is.clear(); // Clear error state
                faceIndices[j] = -1;
                is >> seperator;
            }
            else
                is >> seperator;
        }
        
        newTriangle.Vertices[i] = faceIndices[0];
        newTriangle.UVCoords[i] = faceIndices[1];
        newTriangle.VerticeNormals[i] = faceIndices[2];
    }
    model->Triangles.push_back(newTriangle);
    
//    Triangle currentTriangle = model->Triangles[model->Triangles.size()-1];
//    std::wcout << L"Face vIndex: " << currentTriangle.Vertices[0] << L" " << currentTriangle.Vertices[1] << L" " << currentTriangle.Vertices[2] << std::endl;
//    std::wcout << L"Face nIndex: " << currentTriangle.VerticeNormals[0] << L" " << currentTriangle.VerticeNormals[1] << L" " << currentTriangle.VerticeNormals[2] << std::endl;
//    std::wcout << L"Face tIndex: " << currentTriangle.UVCoords[0] << L" " << currentTriangle.UVCoords[1] << L" " << currentTriangle.UVCoords[2] << std::endl;
}

ModelData* ModelLoader::loadModel(std::string filename)
{
    file = std::wifstream(filename);
    
    gotNormals = false;
    gotTexCoords = false;
    if(file.is_open())
    {
        model = new ModelData();
        while(file.good())
        {
            currentLine = getNextLine();
            
            // material lib to use
            if(currentLine.find(L"mtllib") !=  std::wstring::npos)
                parseMaterialFilename();
            else if(currentLine[0] == 'o')
                parseObjectName();
            else if(currentLine.find(L"vn") == 0) // This has to be checked before the single character 'v' !
                parseNormal();
            else if(currentLine.find(L"vt") == 0) // This has to be checked before the single character 'v' !
                parseNormal();
            else if(currentLine[0] == 'v')
                parseVertex();
            else if(currentLine.find(L"usemtl") == 0)
                parseUsedMaterialName();
            else if (currentLine[0] == 'f')
                parseFace();
        }
    }
    
    file.close();
    
    return model;
}
