//
//  ModelDefinition.h
//  AseLoader
//
//  Created by Sascha Sanzenbacher on 12.03.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

#ifndef AseLoader_ModelDefinition_h
#define AseLoader_ModelDefinition_h

#include <vector>
#include <string>

namespace ModelLoader
{
    //! This struct defines an indexed triable
    struct Triangle
    {
        int     Vertices[3];
        int     UVCoords[3];
        int     VerticeNormals[3];
        int     FaceNormal; // Currently not used !
        int     MaterialID;
        int     SmoothingGroup;
    };
    
    struct ModelData
    {
        std::wstring            Name;
        std::vector<Triangle>   Triangles;
        std::vector<float>      Vertices;
        std::vector<float>      UVCoords;
        std::vector<float>      Normals;
        short                   UVCoordsDimension;
        
        ModelData();
        
        // Informations about the number of elements
        unsigned long numVertices(void);
        
        float* interleavedArray(void);
    };
}
#endif
