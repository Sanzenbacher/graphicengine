//
//  Model.cpp
//  ModelLoader
//
//  Created by Sascha Sanzenbacher on 14.03.13.
//  Copyright (c) 2013 Sascha Sanzenbacher. All rights reserved.
//

#include "ModelData.h"

using namespace ModelLoader;

ModelData::ModelData() : UVCoordsDimension(-1)
{
}

unsigned long ModelData::numVertices(void)
{
    return (Vertices.size() / 3);
}
float* ModelData::interleavedArray(void)
{
    const short posDim = 3;
    const short triangleVerts = 3;
    const short uvDim = (UVCoordsDimension == -1) ? 0 : UVCoordsDimension;
    const short normDim = 3;
    const short numFloatsPerVert = posDim + uvDim + normDim;
    const short numFloatsPerTriangle = numFloatsPerVert*triangleVerts;
    const short numTriangles = Triangles.size();
    const unsigned int numFloats = numTriangles * triangleVerts * numFloatsPerVert;
    float* interleavedArray = new float[numFloats];
    
    for(unsigned int t=0; t<numTriangles; t++)
    {
        Triangle currentTriangle = Triangles[t];
        for(unsigned short v=0; v<3; v++)
        {
            for(unsigned short f=0; f<3; f++)
            {
                // Position
                const short posIndex = t*numFloatsPerTriangle+v*numFloatsPerVert + f;
                interleavedArray[posIndex] = Vertices[(currentTriangle.Vertices[v]-1)*posDim+f];
                
                // UVCoord
                if(uvDim != 0 &&
                   (uvDim != 2 || v != 3)) // v goes from 0 to 3, we don't want that for 2 dimensional texture coordinates !
                {
                    const short uvIndex = t*numFloatsPerTriangle+v*numFloatsPerVert + f + posDim;
                    interleavedArray[uvIndex] = UVCoords[(currentTriangle.VerticeNormals[v]-1)*uvDim+f];
                }
                
                // Normal
                const short normalIndex = t*numFloatsPerTriangle+v*numFloatsPerVert + f + posDim + uvDim;
                interleavedArray[normalIndex] = Normals[(currentTriangle.VerticeNormals[v]-1)*normDim+f];
            }
        }
    }
    
//    for(int i=0; i<numFloats; i++)
//    {
//        if(i % 6 == 0)
//            std::cout << std::endl;
//        std::cout << " " << interleavedArray[i];
//    }
    
    return interleavedArray;
}