//
//  ModelManager.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 11.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__ModelManager__
#define __GraphicsPlayground_Mac__ModelManager__

#include "ResourceManagment/Common/ModelData.h"

class ModelManager
{
public:
    // Desturctor
    virtual ~ModelManager();
    
    // Singleton access
    static ModelManager* getInstance(void);
    
    // Load a texture, when loaded return it
    ModelLoader::ModelData* loadModel(std::string& modelPath);
    
    // Unload texture
    void       unloadModel(std::string& modelPath);
private:
    // Private constructor
    ModelManager();
    
    static ModelManager* s_instance;
};

#endif /* defined(__GraphicsPlayground_Mac__ModelManager__) */
