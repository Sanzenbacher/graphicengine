//
//  TextureDataOSX.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 09.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__TextureDataOSX__
#define __GraphicsPlayground_Mac__TextureDataOSX__

#include <iostream>
#include "ResourceManagment/Common/TextureDataBase.h"

struct TextureDataOSXImpl;

class TextureDataOSX : public TextureDataBase
{
public:
    TextureDataOSX(std::wstring& fullFilename);
    virtual ~TextureDataOSX();
    
    // Return the color of the specified pixel using nearest neighbour
    // @param x  Normalized ranging between 0 and 1
    // @param y  Normalized ranging between 0 and 1
    virtual RgbaColor getPixelColorFloat(float x, float y);
    virtual RgbaColor getPixelColorInt(long x, long y);
    
protected:
    TextureDataOSXImpl* _impl;
};

#endif /* defined(__GraphicsPlayground_Mac__TextureDataOSX__) */
