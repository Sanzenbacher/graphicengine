//
//  ResourceDirectoriesOSX.mm
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 15.08.14.
//
//

#include "ResourceDirectoriesOSX.h"

ResourceDirectoriesOSX::ResourceDirectoriesOSX(void)
{
    NSString *appPath = [[NSBundle mainBundle] resourcePath];
    
    m_shaderDirectory = [appPath cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}