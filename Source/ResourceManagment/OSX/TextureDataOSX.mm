//
//  TextureDataOSX.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 09.07.14.
//
//

#include "TextureDataOSX.h"

#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>

#include <OpenGL/gl.h>

#pragma mark - TextureDataOSXImpl
struct TextureDataOSXImpl
{
    NSImage* nsiTexture;
    NSBitmapImageRep* nsiRepTexture;
};

#pragma  mark - TextureDataOSX

TextureDataOSX::TextureDataOSX(std::wstring& fullFilename) : TextureDataBase(fullFilename)
{
    NSString* filenameNS = [[NSString alloc] initWithBytes:(void*)fullFilename.c_str() length:fullFilename.length()*sizeof(wchar_t) encoding:CFStringConvertEncodingToNSStringEncoding ( kCFStringEncodingUTF32LE )];
    NSImage* nsiTexture = [NSImage imageNamed:filenameNS];
    [filenameNS release];
    
    // Get texture representation
    _impl = new TextureDataOSXImpl();
    _impl->nsiRepTexture = [[NSBitmapImageRep alloc] initWithData:[nsiTexture TIFFRepresentationUsingCompression:NSTIFFCompressionNone factor:1.0]]; // Deprecated in 10.6
    _impl->nsiTexture = [nsiTexture retain];
    
    // Setup attributes
    width = (int32_t)[_impl->nsiRepTexture pixelsWide];
    height = (int32_t)[_impl->nsiRepTexture pixelsHigh];
    hasAlpha = [_impl->nsiRepTexture hasAlpha];
    // GL_RGB16 is different for openGL ES
    format = hasAlpha ? GL_RGBA : GL_RGB;
    bitmapData = [_impl->nsiRepTexture bitmapData];
}
TextureDataOSX::~TextureDataOSX()
{
    [_impl->nsiRepTexture release];
    [_impl->nsiTexture release];
    delete _impl;
}

RgbaColor TextureDataOSX::getPixelColorInt(long x, long y)
{
    RgbaColor resultingColor;
    NSColor* nsColor = [_impl->nsiRepTexture colorAtX:x y:y];
    resultingColor.red = (float)[nsColor redComponent];
    resultingColor.green = (float)[nsColor greenComponent];
    resultingColor.blue = (float)[nsColor blueComponent];
    resultingColor.alpha = (float)[nsColor alphaComponent];
    
    return resultingColor;
}

RgbaColor TextureDataOSX::getPixelColorFloat(float x, float y)
{
    RgbaColor resultingColor;
    
    NSInteger nsX = (NSInteger)floor(x*width);
    NSInteger nsY = (NSInteger)floor(y*height);
    
    return getPixelColorInt(nsX, nsY);
}