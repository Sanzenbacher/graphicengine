//
//  ResourceDirectoriesOSX.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 15.08.14.
//
//

#ifndef __GraphicsPlayground_Mac__ResourceDirectoriesOSX__
#define __GraphicsPlayground_Mac__ResourceDirectoriesOSX__

#include "ResourceManagment/Common/ResourceDirectories.h"

class ResourceDirectoriesOSX : public ResourceDirectories
{
public:
    ResourceDirectoriesOSX(void);
};
#endif /* defined(__GraphicsPlayground_Mac__ResourceDirectoriesOSX__) */
