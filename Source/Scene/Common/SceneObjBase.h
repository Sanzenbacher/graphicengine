//
//  SceneObjBase.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 17.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_SceneObjBase_h
#define macterrain_SceneObjBase_h

#include "Manipulator.h"
#include "Identifier.h"

// Forward declarations
class SceneObjectProperties;
class SceneObjBase;

// Typedefs
typedef std::vector<SceneObjBase*> SceneObjBaseVector;

// This object offers basic functionality that is needed for every object which is managed by SceneManager.
class SceneObjBase : public Manipulator
{
public:
    // Con-/Destructors
    SceneObjBase(SceneObjectProperties sceneObjDesc);
    virtual ~SceneObjBase();
    
    Identifier identifier(void);
    std::shared_ptr<SceneObjectProperties> properties(void);
protected:
    std::shared_ptr<SceneObjectProperties> _sceneObjectProperties;
};

#endif
