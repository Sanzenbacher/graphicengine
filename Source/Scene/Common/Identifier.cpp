//
//  Identifier.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 25.06.14.
//
//

#include "Identifier.h"

Identifier::Identifier()
{
    
}

Identifier::Identifier(void* object)
{
    _identifier = (long)object;
}

bool Identifier::operator==(Identifier const& rhs)
{
    return _identifier == rhs._identifier;
}