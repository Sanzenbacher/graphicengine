//
//  Manipulator.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 24.06.14.
//
//

#include "Manipulator.h"

#include "Scene/Common/ManipulationHandler.h"

void Manipulator::setManipulationHandler(ManipulationHandler* handler)
{
    _manipulationHandler = handler;
}