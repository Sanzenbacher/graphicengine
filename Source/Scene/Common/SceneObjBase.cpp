//
//  SceneObjBase.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 17.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "SceneObjBase.h"

#include "SceneObjectProperties.h"

#pragma mark Con-/Destructors
SceneObjBase::SceneObjBase(SceneObjectProperties sceneObjectProperties) :
    _sceneObjectProperties(new SceneObjectProperties(sceneObjectProperties))
{
    _sceneObjectProperties->identifier = Identifier(this);
}
SceneObjBase::~SceneObjBase()
{
}

#pragma mark Getter/Setter
Identifier SceneObjBase::identifier(void)
{
    return _sceneObjectProperties->identifier;
}
std::shared_ptr<SceneObjectProperties> SceneObjBase::properties(void)
{
    std::shared_ptr<SceneObjectProperties> propertiesCopy(new SceneObjectProperties(*_sceneObjectProperties));
    return propertiesCopy;
}