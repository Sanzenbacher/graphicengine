//
//  ScObj_SimpleCamera.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 18.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "ScObj_SimpleCamera.h"

#include <iostream>

#include "ManipulationHandler.h"
#include "SceneObjectProperties.h"

#pragma static consts
const std::wstring ScObj_SimpleCamera::PropertyPresentationPerspectiveTransformation = L"PropertyPresentationPerspectiveTransformation"; // slm::mat4


#pragma mark Con-/Destructor
ScObj_SimpleCamera::ScObj_SimpleCamera(SceneObjectProperties initialProperties) : SceneObjBase(initialProperties)
{
    // Initial position
    positionVector = slm::vec3(5.5f,3.0f,5.0f);
    rotationVector = slm::vec2(0.0f, 2.2f);
    
//    slm::mat4 initialTransformation = ttl::var::get<slm::mat4>(initialProperties.presentation[PropertyPresentationTransformation]);
    
    // Calculate position and rotation vector
//    positionVector = initialTransformation[3].xyz();
//    slm::vec4 transformedX = initialTransformation*slm::vec4(1., 0., 0., 1.);
//    transformedX.w = 0.;
//    float dotY = slm::dot(slm::normalize(transformedX), slm::vec4(1., 0., 0., 0.));
//    float angleY = acosf(slm::clamp(dotY, -1.0, 1.0));
//    slm::vec4 transformedY = initialTransformation*slm::vec4(0., 1., 0., 1.);
//    transformedY.w = 0.;
//    float dotX = slm::dot(slm::normalize(transformedY), slm::vec4(0., 1., 0., 0.));
//    float angleX = acosf(slm::clamp(dotX, -1.0, 1.0));
//    rotationVector = slm::vec2(angleX, angleY);
    
    _sceneObjectProperties->presentation[PropertyPresentationManipulator] = (Manipulator*)this;
    _sceneObjectProperties->presentation[PropertyPresentationType] = PresentationType::PT_TYPE_CAMERA;
}
ScObj_SimpleCamera::~ScObj_SimpleCamera()
{
    
}

#pragma mark Movement
void ScObj_SimpleCamera::moveForward(float ammount)
{
    slm::vec3 deltaVec = slm::vec3(0.0f, 0.0f, ammount);
    updatePosition(deltaVec);
    updateTransformationMatrix();
}
void ScObj_SimpleCamera::moveBackward(float ammount)
{
    slm::vec3 deltaVec = slm::vec3(0.0f, 0.0f, -ammount);
    updatePosition(deltaVec);
    updateTransformationMatrix();
}
void ScObj_SimpleCamera::moveLeft(float ammount)
{
    slm::vec3 deltaVec = slm::vec3(ammount, 0.0f, 0.0f);
    updatePosition(deltaVec);
    updateTransformationMatrix();
}
void ScObj_SimpleCamera::moveRight(float ammount)
{
    slm::vec3 deltaVec = slm::vec3(-ammount, 0.0f, 0.0f);
    updatePosition(deltaVec);
    updateTransformationMatrix();
}
void ScObj_SimpleCamera::rotateX(float ammount)
{
    rotationVector.x += ammount;
    updateTransformationMatrix();
}
void ScObj_SimpleCamera::rotateY(float ammount)
{
    rotationVector.y += ammount;
    updateTransformationMatrix();
}


#pragma mark Update render object internal
void ScObj_SimpleCamera::updateTransformationMatrix(void)
{
    slm::mat4 rotationMatrix = slm::rotationX(rotationVector.x) * slm::rotationY(rotationVector.y);
    
    slm::vec4 originalTarget = slm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
    slm::vec4 rotatedTarget = originalTarget * rotationMatrix;
    slm::vec4 posVec = slm::vec4(positionVector, 1.0f);
    slm::vec4 finalTarget = posVec + rotatedTarget;
    
    slm::vec4 originalUp = slm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
    slm::vec4 rotatedUp = originalUp * rotationMatrix;
    
    // targetAtRH or lookAtRH ?
    slm::mat4 finalTransformation = slm::lookAtRH(positionVector, finalTarget.xyz(), rotatedUp.xyz());
    _manipulationHandler->manipulatePropertyWithName(ManipulationHandler::cMPTransformationMatrix, (void* const)&finalTransformation);
}
void ScObj_SimpleCamera::updatePosition(slm::vec3 deltaVec)
{
    slm::mat4 rotationMatrix = slm::rotationX(rotationVector.x) * slm::rotationY(rotationVector.y);
    slm::vec4 rotatedVector = slm::vec4(deltaVec, 1.0f) * rotationMatrix;
    positionVector += rotatedVector.xyz();
}
