//
//  ManipulationHandler.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 24.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__ManipulationHandler__
#define __GraphicsPlayground_Mac__ManipulationHandler__

#include <iostream>
#include <string>

class ManipulationHandler
{
public:
    static std::wstring* const cMPTransformationMatrix;
    
    virtual bool manipulatePropertyWithName(std::wstring* const propertyName, void* propertyValue) = 0;
    
};

#endif /* defined(__GraphicsPlayground_Mac__ManipulationHandler__) */
