//
//  ScObj_Triangle.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.06.14.
//
//

#ifndef GraphicsPlayground_Mac_ScObj_Triangle_h
#define GraphicsPlayground_Mac_ScObj_Triangle_h

#include "SceneObjBase.h"

class SceneObjectProperties;

class ScObj_Triangle : public SceneObjBase
{
public:
    static const std::wstring PropertyPresentationTransformationMatrix; // slm::mat4
    
    ScObj_Triangle(SceneObjectProperties parameters);
    ~ScObj_Triangle();
};


#endif
