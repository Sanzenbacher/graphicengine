//
//  ScObj_StaticMesh.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 18.07.14.
//
//

#include "ScObj_StaticMesh.h"

#include "SceneObjectProperties.h"

const std::wstring ScObj_StaticMesh::PropertyPresentationStaticMeshFilename; // std::string

ScObj_StaticMesh::ScObj_StaticMesh(SceneObjectProperties initialParameters) : SceneObjBase(initialParameters)
{
    _sceneObjectProperties->presentation[PropertyPresentationType] = PresentationType::PT_TYPE_STATIC_MESH;
}
ScObj_StaticMesh::~ScObj_StaticMesh()
{
    
}