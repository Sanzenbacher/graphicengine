//
//  SceneManager.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 17.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "SceneManager.h"

#include <algorithm>

#include "RendererBase/Common/RenderObject.h"
#include "SceneObjBase.h"
#include "ScObj_SimpleCamera.h"

#pragma mark Con-/Destructors
SceneManager::SceneManager()
{
    _cameraObject = NULL;
}
SceneManager::~SceneManager()
{
    for(SceneObjBaseVector::iterator it = _sceneObjects.begin();
        it != _sceneObjects.end(); it++)
        unregisterObject(*it);
    
    delete _cameraObject;
}

#pragma mark Object managment
void SceneManager::registerCamera(ScObj_SimpleCamera* cameraObject)
{
    if(_cameraObject)
        delete _cameraObject;
    
    _cameraObject = cameraObject;
}
void SceneManager::registerObject(SceneObjBase* newObject)
{
    _sceneObjects.push_back(newObject);
    
    _newVisibleObjects.push_back(newObject);
}
void SceneManager::unregisterObject(SceneObjBase* remObject)
{
    SceneObjBaseVector::iterator sceneObjIt =
        std::find( _sceneObjects.begin(), _sceneObjects.end(), remObject );
    if(sceneObjIt != _newVisibleObjects.end())
    {
        _sceneObjects.erase(sceneObjIt);
    }
    
    // When the object is still in the newVisibleObjects vector, remove it.
    sceneObjIt = std::find( _newVisibleObjects.begin(), _newVisibleObjects.end(), remObject );
    if(sceneObjIt != _newVisibleObjects.end())
    {
        _newVisibleObjects.erase(sceneObjIt);
    }
    _obsoleteObjects.push_back(*sceneObjIt);
}
SceneObjBaseVector SceneManager::queryNewVisibleObjects()
{
    SceneObjBaseVector result = _newVisibleObjects;
    _newVisibleObjects.clear();
    return result;
}
SceneObjBaseVector SceneManager::queryObsoleteObjects()
{
    SceneObjBaseVector result = _obsoleteObjects;
    _obsoleteObjects.clear();
    return result;
}