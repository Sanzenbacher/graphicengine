//
//  SceneObjectProperties.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 04.07.14.
//
//

#include "SceneObjectProperties.h"

#include "Identifier.h"

const std::wstring PropertyPresentationManipulator = L"PropertyPresentationManipulator";
const std::wstring PropertyPresentationType = L"PropertyPresentationType";
const std::wstring PropertyPresentationTextures = L"PropertyPresentationTextures"; // std::vector<std::wstring>
const std::wstring PropertyPresentationTransformation = L"PropertyPresentationTransformation"; // slm::mat4

SceneObjectProperties::SceneObjectProperties(void)
{
}