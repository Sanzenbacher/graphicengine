//
//  Identifier.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 25.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__Identifier__
#define __GraphicsPlayground_Mac__Identifier__

#include <iostream>

class Identifier
{
public:
    
    Identifier();
    Identifier(void* object);
    bool operator==(Identifier const& rhs);
    
protected:
    long _identifier;
};

#endif /* defined(__GraphicsPlayground_Mac__Identifier__) */
