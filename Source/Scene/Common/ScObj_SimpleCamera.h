//
//  ScObj_SimpleCamera.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 18.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_ScObj_SimpleCamera_h
#define macterrain_ScObj_SimpleCamera_h

#include "SceneObjBase.h"

#include "SceneObjectProperties.h"

// This class represents a simple camera
class ScObj_SimpleCamera : public SceneObjBase
{
public:
    static const std::wstring PropertyPresentationPerspectiveTransformation; // slm::mat4
    
    // Con-/Destructor
    ScObj_SimpleCamera(SceneObjectProperties initialProperties);
    virtual ~ScObj_SimpleCamera();
    
    // Functions that realize movements
    void moveForward(float ammount);
    void moveBackward(float ammount);
    void moveLeft(float ammount);
    void moveRight(float ammount);
    void rotateX(float ammount);
    void rotateY(float ammount);
    
private:
    slm::vec3 positionVector;
    slm::vec2 rotationVector;
    slm::mat4 transformation;
    
    // Update render object internal
    void updateTransformationMatrix(void);
    void updatePosition(slm::vec3 deltaVec);
};

#endif
