//
//  ScObj_SimpleTerrain.cpp
//  macterrain
//
//  Created by Sascha Sanzenbacher on 05.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "ScObj_SimpleTerrain.h"

#include "ResourceManagment/Common/TextureManager.h"

#include "SceneObjectProperties.h"

const std::wstring ScObj_SimpleTerrain::PropertyPresentationDetailFactor = L"PropertyPresentationDetailFactor"; // float
const std::wstring ScObj_SimpleTerrain::PropertyPresentationTotalWidth = L"PropertyPresentationTotalWidth"; // float
const std::wstring ScObj_SimpleTerrain::PropertyPresentationTotalDepth = L"PropertyPresentationTotalDepth"; // float
const std::wstring ScObj_SimpleTerrain::PropertyPresentationTotalHeight = L"PropertyPresentationTotalHeight"; // float
const std::wstring ScObj_SimpleTerrain::PropertyPresentationHeightMapFilename = L"PropertyPresentationHeightMapFilename"; // std::wstring

#pragma mark Con-/Destructor
ScObj_SimpleTerrain::ScObj_SimpleTerrain(SceneObjectProperties initialProperties) : SceneObjBase(initialProperties)
{
    _sceneObjectProperties->presentation[PropertyPresentationType] = PresentationType::PT_TYPE_STATIC_HIGHTMAP;
}
ScObj_SimpleTerrain::~ScObj_SimpleTerrain()
{
    
}


