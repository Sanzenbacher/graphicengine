//
//  Manipulator.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 24.06.14.
//
//

#ifndef __GraphicsPlayground_Mac__Manipulator__
#define __GraphicsPlayground_Mac__Manipulator__

#include <iostream>

class ManipulationHandler;

class Manipulator
{
public:
    void setManipulationHandler(ManipulationHandler* handler);
    
protected:
    ManipulationHandler* _manipulationHandler;
};

#endif /* defined(__GraphicsPlayground_Mac__Manipulator__) */
