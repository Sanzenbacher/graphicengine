//
//  SceneManager.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 17.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_SceneManager_h
#define macterrain_SceneManager_h

#include <vector>

#include "SceneObjBase.h"

// Forward declarations
class RenderObject;
class SceneObjBase;
class ScObj_SimpleCamera;

// This class holds all objects that exist in one scene.
// Loading and rendering are automatically managed by this class.
// It provides functionality to registering and unregister scene objects. 
class SceneManager
{
public:
    //Default con-/destructors
    SceneManager();
    ~SceneManager();
    
    // Register an object in the scene, loading and rendering will be handled automatically
    // @param SceneObjBase  The scene Object that will be registered
    void registerCamera(ScObj_SimpleCamera* cameraObject);
    void registerObject(SceneObjBase* newObject);
    void unregisterObject(SceneObjBase* remObject);
    
    // Returns a vector which contains all new visible RenderObjects
    SceneObjBaseVector queryNewVisibleObjects(void);
    SceneObjBaseVector queryObsoleteObjects(void);
    
private:
    SceneObjBaseVector _sceneObjects;
    ScObj_SimpleCamera* _cameraObject;
    
    SceneObjBaseVector _newVisibleObjects;
    SceneObjBaseVector _obsoleteObjects;
};

#endif
