//
//  ScObj_Triangle.cpp
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 20.06.14.
//
//

#include "ScObj_Triangle.h"

#include "SceneObjectProperties.h"

const std::wstring ScObj_Triangle::PropertyPresentationTransformationMatrix = L"PropertyPresentationTransformationMatrix"; // slm::mat4

ScObj_Triangle::ScObj_Triangle(SceneObjectProperties initialProperties) : SceneObjBase(initialProperties)
{
    _sceneObjectProperties->presentation[PropertyPresentationType] = PresentationType::PT_TYPE_TRIANGLE;
}

ScObj_Triangle::~ScObj_Triangle()
{
    
}