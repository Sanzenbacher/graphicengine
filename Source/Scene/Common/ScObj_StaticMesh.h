//
//  ScObj_StaticMesh.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 18.07.14.
//
//

#ifndef __GraphicsPlayground_Mac__ScObj_StaticMesh__
#define __GraphicsPlayground_Mac__ScObj_StaticMesh__

#include "SceneObjBase.h"

class ScObj_StaticMesh : public SceneObjBase
{
public:
    
    static const std::wstring PropertyPresentationStaticMeshFilename; // std::string
    
    ScObj_StaticMesh(SceneObjectProperties initialParameters);
    virtual ~ScObj_StaticMesh();
};

#endif /* defined(__GraphicsPlayground_Mac__ScObj_StaticMesh__) */
