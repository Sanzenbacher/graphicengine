//
//  ScObj_SimpleTerrain.h
//  macterrain
//
//  Created by Sascha Sanzenbacher on 05.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef macterrain_ScObj_SimpleTerrain_h
#define macterrain_ScObj_SimpleTerrain_h

#include "SceneObjBase.h"

// This class represents a terrain-object in the scene.
// The number of triangles in the x-Axis is calculated by multiplying the 
// detailFactor with 32. All triangles will have the same width and Depth, which 
// will result in different numbers of triangles for the x- and z-axis.
class ScObj_SimpleTerrain : public SceneObjBase
{
public:
    
    static const std::wstring PropertyPresentationDetailFactor; // float
    static const std::wstring PropertyPresentationTotalWidth; // float
    static const std::wstring PropertyPresentationTotalDepth; // float
    static const std::wstring PropertyPresentationTotalHeight; // float
    static const std::wstring PropertyPresentationHeightMapFilename; // std::wstring
    
    // Con-/Destructor
    ScObj_SimpleTerrain(SceneObjectProperties initialParameters);
    virtual ~ScObj_SimpleTerrain();
};

#endif
