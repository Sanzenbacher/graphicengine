//
//  SceneObjectProperties.h
//  GraphicsPlayground_Mac
//
//  Created by Sascha Sanzenbacher on 04.07.14.
//
//

#ifndef GraphicsPlayground_Mac_SceneObjectProperties_h
#define GraphicsPlayground_Mac_SceneObjectProperties_h

#include "Identifier.h"

// Forward declarations
class Manipulator;
enum class PresentationType;

// List of property keys
extern const std::wstring PropertyPresentationManipulator; // Manipulator*
extern const std::wstring PropertyPresentationType; // PresentationType
extern const std::wstring PropertyPresentationTextures; // std::vector<std::wstring>
extern const std::wstring PropertyPresentationTransformation; // slm::mat4

// Type definitions
typedef std::map<std::wstring, ttl::var::variant<int32_t, float, bool, std::wstring, std::string, std::vector<std::wstring>, Manipulator*, slm::mat4, PresentationType>> PropertyMapType;

enum class PresentationType
{
    PT_TYPE_TRIANGLE,
    PT_TYPE_STATIC_MESH,
    PT_TYPE_STATIC_HIGHTMAP,
    PT_TYPE_CAMERA
};

struct SceneObjectProperties
{
public:
    SceneObjectProperties(void);
    
    PropertyMapType presentation;
    Identifier      identifier;
};

#endif
